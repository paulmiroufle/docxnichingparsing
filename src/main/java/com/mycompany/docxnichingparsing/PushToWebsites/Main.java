    package com.mycompany.docxnichingparsing.PushToWebsites;

    import com.sun.corba.se.impl.orbutil.closure.Constant;
    import net.bican.wordpress.CustomField;
    import net.bican.wordpress.Post;
    import net.bican.wordpress.Wordpress;
    import net.bican.wordpress.exceptions.InsufficientRightsException;
    import net.bican.wordpress.exceptions.InvalidArgumentsException;
    import net.bican.wordpress.exceptions.ObjectNotFoundException;
    import org.jsoup.Jsoup;
    import org.jsoup.nodes.Document;
    import org.jsoup.nodes.Element;
    import org.jsoup.select.Elements;
    import redstone.xmlrpc.XmlRpcFault;

    import java.net.MalformedURLException;
    import java.nio.charset.StandardCharsets;
    import java.sql.*;
    import java.util.ArrayList;
    import java.util.Arrays;
    import java.util.HashMap;
    import java.util.List;

    public class Main {
        private static String test="salut ca va?";
        private static String DB_NAME="storeamazon";
        private static String DB_NAME0="storeamazon3";
        private static String DB_HOST="localhost";
        private static String DB_USER="root";
        private static String DB_PASS="6#hW^Ockal2e7@oYckcIxBmFL&5B*";
        private static Connection conn;
        private static Connection conn0;
        private static String login="adminLaMif";
        private static String password="3t6qNzPLRam2!bb0X0Jommzc";

        public static void init() throws SQLException, ClassNotFoundException {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = DriverManager.getConnection(
                    "jdbc:mysql://" + DB_HOST + ":3306/" + DB_NAME + "?serverTimezone=UTC&useSSL=false", DB_USER, DB_PASS);
            String query = "SET NAMES 'utf8mb4'";
            Statement stmt = conn.createStatement();
            ((Statement) stmt).executeQuery(query);
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn0 = DriverManager.getConnection(
                    "jdbc:mysql://" + DB_HOST + ":3306/" + DB_NAME0 + "?serverTimezone=UTC&useSSL=false", DB_USER, DB_PASS);
             query = "SET NAMES 'utf8mb4'";
             stmt = conn.createStatement();
            ((Statement) stmt).executeQuery(query);
        }
        public static void close() throws SQLException{
            conn.close();
        }

        public static void old(String[] args) throws InsufficientRightsException, InvalidArgumentsException, XmlRpcFault, ObjectNotFoundException, MalformedURLException {
            Wordpress client =  new Wordpress(login,password, "https://"+"chaussuresde-securite.fr"+"/xmlrpc.php");
            List<CustomField> customFields=new ArrayList<>();
            Post post=client.getPost(1021);
            CustomField cf=new CustomField();
            post=new Post();
            cf.setKey("testKey");
            cf.setValue("testValue");
            customFields.add(cf);
            post.setCustom_fields(customFields);
            post.setPost_title("hello");
            client.editPost(1021,post);
            System.out.println("hello");
        }
        public static void main(String[] args) throws SQLException, ClassNotFoundException, InvalidArgumentsException, XmlRpcFault, InsufficientRightsException {
            makeit();
            //init();
            //setAsPublished(conn,"B0000223IY",0);
        }
        public static void makeit() throws SQLException, ClassNotFoundException, XmlRpcFault, InsufficientRightsException, InvalidArgumentsException {
            int publishCount=0;
            init();
            String[] asins=Constants.asinCourString.split("\n");
            //ici caler une query sur les unique ASINS exraits de la bdd...ou un static String
            int limit=0;
            for(String asinCour:asins) {
                //asinCour="B0000644FC";
                //asinCour="B0001NDGE8";
                if(asinCour.contains("B07DQYZDLR"))
                    continue;
                limit++;
                //System.out.println(asinCour);
              //  if(limit>3)
               //     continue;
                String query = "select *\n" +
                        "from storecorrespproductreviews s1, storeproductreviews s2\n" +
                        "where s1.asin = s2.asin\n" +
                        "and s1.asin = '"+asinCour+"'\n" +
                        "and s2.isDone is NULL " +
                        "order by s1.asin ,s1.id ";
                PreparedStatement stmt = conn.prepareStatement(query);
                HashMap<String,Integer> h=new HashMap<>();
                try {
                    ResultSet rs4 = stmt.executeQuery(query);
                    int cour=0;
                    int prevId=0;
                    while (rs4.next()) {
                        //traiter la row
                        String domain = rs4.getString("domain");
                        if(domain.contains("tests-vasque-à-poser.fr"))
                            continue;
                        String asin = rs4.getString("asin");
                        String firstTitle = Jsoup.parse(rs4.getString("firstTitle")).text();
                        String secondTitle = Jsoup.parse(rs4.getString("secondTitle")).text();
                        String intro = rs4.getString("intro");
                        String metaDesc = rs4.getString("metaDescription");
                        String mainText = rs4.getString("mainText");
                        String advantages = rs4.getString("advantages");
                        String drawbacks = rs4.getString("drawbacks");
                        int id = rs4.getInt("id");
                        int num = rs4.getInt("num");
                        //System.out.println("-->"+id+" "+Jsoup.parse(advantages).select("p").get(0));
                        if(h.get(asin)==null) {
                            if(cour<=num && prevId==0) {
                                //System.out.println("on publie "+ id+" "+cour + " "+num);
                                cour++;
                                prevId=id;
                                h.put(asin,id);
                                //on publie sur wp
                            }else {
                                continue;
                            }
                        }else {
                            if(id!=prevId && cour<=num) {
                                //on publie
                                //System.out.print ("*** on publie "+ id+" "+cour + " "+num +"***");
                                prevId=id;
                                cour++;
                            }else {
                                continue;
                            }
                        }
                        //poster sur le bon WP
                        //connect to WP
                        int jump=1;
                        System.out.println();
                        System.out.print(asin);
                        if(jump==0)
                            continue;
                        System.out.print("*** https://"+ domain+"/?p=" + id +"***");
                        Wordpress client =  new Wordpress(login,password, "https://"+domain+"/xmlrpc.php");
                        Post post=new Post();
                        List<CustomField> customFields=new ArrayList<>();
                        if(customFields!=null&&jump==1) {
                            //System.out.println("size non null");
                            List<String> keys=new ArrayList<>();
                            keys.add(Constants.seoTitle);
                            keys.add(Constants.seoMetaDesc);
                            keys.add(Constants.mainText);
                            keys.add(Constants.intro);
                            keys.add(Constants.asin);
                            keys.add(Constants.pro1);
                            keys.add(Constants.pro2);
                            keys.add(Constants.pro3);
                            keys.add(Constants.con1);
                            keys.add(Constants.con2);
                            keys.add(Constants.con3);
                            keys.add(Constants.technicals);
                            Document html=Jsoup.parse(mainText);
                            Elements elements = html.select("span.titre2");
                            elements.tagName("h2");
                            //mainText=html.select("body").html();
                            //System.out.println("jsoup "+ advantages);
                            Elements pros=Jsoup.parse(advantages).select("p");
                            Elements cons=Jsoup.parse(drawbacks).select("p");
                            for (String key : keys) {
                                CustomField c=new CustomField();
                                c.setKey(key);
                                //System.out.println(key);
                                if (Constants.seoTitle.contains(key)) {
                                    c.setValue(secondTitle);
                                }
                                if (Constants.seoMetaDesc.contains(key)) {
                                    c.setValue(metaDesc);
                                }
                                if (Constants.intro.contains(key)) {
                                    c.setValue(intro);
                                }
                                if (Constants.mainText.contains(key)) {
                                    c.setValue(JsoupPlaying.insertImagesIntoMainTextProductReview(html,asin,secondTitle));
                                    //c.setValue(html.html());
                                }
                                if (Constants.pro1.contains(key) && pros.size()>0) {
                                    c.setValue(pros.get(0).text());
                                }
                                if (Constants.pro2.contains(key)&& pros.size()>1) {
                                    c.setValue(pros.get(1).text());
                                }
                                if (Constants.pro3.contains(key)&& pros.size()>2) {
                                    c.setValue(pros.get(2).text());
                                }
                                if (Constants.con1.contains(key)&& cons.size()>0) {
                                    c.setValue(cons.get(0).text());
                                }
                                if (Constants.con2.contains(key)&& cons.size()>1) {
                                    c.setValue(cons.get(1).text());
                                }
                                if (Constants.con3.contains(key)&& cons.size()>2) {
                                    c.setValue(cons.get(2).text());
                                }
                                if (Constants.asin.contains(key)) {
                                    c.setValue(asin);
                                }
                                if (Constants.technicals.contains(key)) {
                                    String query0 = "select html\n" +
                                            "from storepages s\n" +
                                            "where s.asin='" +asin+"'"+
                                            "limit 1 ;";
                                    Statement stmt0 = conn0.prepareStatement(query0);
                                    String technicals = "";
                                    try {
                                        ResultSet rs = stmt0.executeQuery(query0);
                                        while (rs.next()) {
                                            //System.out.println(rs.getString(1));
                                            //#prodDetails
                                            List<Element> l=Jsoup.parse(rs.getString(1)).select("#prodDetails table");
                                            if(l.size()>1) {
                                                System.out.print("*** TROUVE 1 -- asin = "+" "+asin+" ***");
                                                String input=l.get(0).html();
                                                String output = new String(input.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);

                                                technicals = output;//l.get(0).html();
                                                //System.out.println(output);

                                                //return;
                                            }else{
                                                technicals=rs.getString(1);
                                                System.out.print("*** TROUVE 2 -- asin = "+" "+asin+" ***");
                                            }
                                        }
                                    } finally {
                                    stmt0.close();
                                     }
                                    //System.out.println(technicals);
                                    c.setValue("<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\">"+technicals+"</table>");
                                }
                                customFields.add(c);
                            }
                            post.setCustom_fields(customFields);
                            post.setPost_title(firstTitle);
                            post.setPost_status("publish");
                            try {
                                System.out.print("*** PUBLISHED ! "+id + "-->" + client.editPost(id, post) + "| " + publishCount++ +" ***");
                                setAsPublished(conn,asin,num);
                            }
                            catch (Exception e){
                                System.out.println("OH SHIT "+domain+" "+asinCour);
                                e.printStackTrace();
                            }
                        }
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } finally {
                    stmt.close();
                }
            }
        }

        private static void setAsPublished(Connection conn, String asin, int num) throws SQLException {
            String query="update storeproductreviews s\n" +
                    "set isDone=1\n" +
                    "where s.ASIN=?\n" +
                    "and s.num=?;";
            PreparedStatement ps=conn.prepareStatement(query);
            ps.setString(1,asin);
            ps.setString(2,new Integer(num).toString());
            ps.executeUpdate();
        }
    }
