package com.mycompany.docxnichingparsing.PushToWebsites;

public class Constants {

    static String technicals="technicals";
    static String seoTitle="_yoast_wpseo_title";
    static String seoMetaDesc="_yoast_wpseo_metadesc";
    static String intro="intro";
    static String mainText="corps_du_test";
    static String pro1="pro1";
    static String pro2="pro2";
    static String pro3="pro3";
    static String con1="con1";
    static String con2="con2";
    static String con3="con3";
    static String asin="asin";
    static String asinCourString="1445433753\n" +
            "273720383X\n" +
            "B0000223IY\n" +
            "B00004RDF0\n" +
            "B00004YOH7\n" +
            "B000056NOA\n" +
            "B000056NOD\n" +
            "B0000614UT\n" +
            "B0000644FC\n" +
            "B0000DGQV1\n" +
            "B0001D1Q0Y\n" +
            "B0001E3RZU\n" +
            "B0001E41W8\n" +
            "B0001IP2NQ\n" +
            "B0001IP2O0\n" +
            "B0001NDGE8\n" +
            "B0002DHN6Y\n" +
            "B0002DI4IU\n" +
            "B0002GV1MS\n" +
            "B0002TTRMQ\n" +
            "B0002TTRO4\n" +
            "B0002TTROE\n" +
            "B0006JJFOC\n" +
            "B0006JJFS8\n" +
            "B00074EJTM\n" +
            "B00074ELQI\n" +
            "B0007XXHHI\n" +
            "B0008G2V6M\n" +
            "B0008GHE1E\n" +
            "B000A6Z9WS\n" +
            "B000EP8DDW\n" +
            "B000FDVIAI\n" +
            "B000FMPW8I\n" +
            "B000G3HT28\n" +
            "B000GPILSW\n" +
            "B000GT88ZE\n" +
            "B000K0LMZ2\n" +
            "B000KENQFC\n" +
            "B000KKGYD2\n" +
            "B000MXI0UC\n" +
            "B000MXI19M\n" +
            "B000N9AV5M\n" +
            "B000ND5C9I\n" +
            "B000OY4W0Q\n" +
            "B000OY6LS2\n" +
            "B000P37KGO\n" +
            "B000P38ULI\n" +
            "B000PJ8CS8\n" +
            "B000PV9IDE\n" +
            "B000PXB04C\n" +
            "B000PXHJ60\n" +
            "B000PZHFH6\n" +
            "B000RPITKQ\n" +
            "B000RVWQIQ\n" +
            "B000RVWQOA\n" +
            "B000RVYIIC\n" +
            "B000RVYIMS\n" +
            "B000RXTLDC\n" +
            "B000S6C16C\n" +
            "B000SHVRO8\n" +
            "B000T5L3U2\n" +
            "B000TCOT1A\n" +
            "B000TGGIB0\n" +
            "B000TQEXDK\n" +
            "B000UYTJW6\n" +
            "B000UYUMN6\n" +
            "B000UYWOXM\n" +
            "B000VX6YBA\n" +
            "B000VZ4VRW\n" +
            "B000W105C0\n" +
            "B000XG20J0\n" +
            "B000XG20VI\n" +
            "B000Y00WNQ\n" +
            "B0010WJZK8\n" +
            "B00119LJ7W\n" +
            "B0012342US\n" +
            "B0012MBPCW\n" +
            "B0012RGATK\n" +
            "B0012RON94\n" +
            "B0012RSDI6\n" +
            "B0012RU23A\n" +
            "B0013BHDSW\n" +
            "B0013E4AKI\n" +
            "B0013F2VVM\n" +
            "B0013F69OW\n" +
            "B0013H5M1Q\n" +
            "B0013LNAE8\n" +
            "B0013XXY10\n" +
            "B001497KAY\n" +
            "B001499CGO\n" +
            "B0015492AE\n" +
            "B0015S7OSW\n" +
            "B0016ITG2I\n" +
            "B001AH7532\n" +
            "B001B2N9FY\n" +
            "B001B46W6K\n" +
            "B001BSC3FU\n" +
            "B001DLU60E\n" +
            "B001DZM482\n" +
            "B001DZTPD4\n" +
            "B001EPF9WO\n" +
            "B001F02LPQ\n" +
            "B001FK4YC4\n" +
            "B001HMU86Q\n" +
            "B001IONAF4\n" +
            "B001MSV8RS\n" +
            "B001N02ACW\n" +
            "B001NP3OD6\n" +
            "B001O7SDNE\n" +
            "B001PGVXEK\n" +
            "B001QOGSGY\n" +
            "B001QTVT4A\n" +
            "B001RB2L8U\n" +
            "B001RB2L9O\n" +
            "B001RCUGBI\n" +
            "B001RTU4DQ\n" +
            "B001TOKHIG\n" +
            "B001TOKHKO\n" +
            "B001TUYTVG\n" +
            "B001U3XVMA\n" +
            "B001VKSAAK\n" +
            "B001W0QKYW\n" +
            "B002007HQI\n" +
            "B0020VE1W0\n" +
            "B0021XH5RU\n" +
            "B00243DBAM\n" +
            "B002445IZW\n" +
            "B0024QVD86\n" +
            "B0025UHKNS\n" +
            "B00261EMQO\n" +
            "B0029LH358\n" +
            "B002BRJ3HQ\n" +
            "B002DEMEAU\n" +
            "B002EX4VB0\n" +
            "B002FVYP08\n" +
            "B002HMNGFA\n" +
            "B002HWRI90\n" +
            "B002HWRIAO\n" +
            "B002HWRIBI\n" +
            "B002HWRIEA\n" +
            "B002ID1VMI\n" +
            "B002JCSCUW\n" +
            "B002JKUXCO\n" +
            "B002L1KJRU\n" +
            "B002LARSH0\n" +
            "B002NU6MMU\n" +
            "B002PBU0KC\n" +
            "B002PEQ6M0\n" +
            "B002QRYEBQ\n" +
            "B002QZW85W\n" +
            "B002TLS75I\n" +
            "B002USHAKI\n" +
            "B002USZWGM\n" +
            "B002UTDRE0\n" +
            "B002WIMQQ4\n" +
            "B002YIBU3M\n" +
            "B002ZB74WO\n" +
            "B002ZB74XS\n" +
            "B0030DH1WE\n" +
            "B0030DH1WO\n" +
            "B0030DH1Y2\n" +
            "B00310R798\n" +
            "B00314NSX8\n" +
            "B00331YZ2W\n" +
            "B00336WGDC\n" +
            "B0034AP5L2\n" +
            "B0034BBM40\n" +
            "B0034GZM78\n" +
            "B0034YT3QG\n" +
            "B0035A8CFC\n" +
            "B0036EFAV6\n" +
            "B0036KRW3O\n" +
            "B0037AZLXQ\n" +
            "B0037B3DQW\n" +
            "B0037B5BGW\n" +
            "B0037B5BHG\n" +
            "B0037B5BMQ\n" +
            "B003922VGM\n" +
            "B003922VIU\n" +
            "B0039O455K\n" +
            "B0039YP0CM\n" +
            "B0039YP0SQ\n" +
            "B0039YP0UE\n" +
            "B003A66V1I\n" +
            "B003BVK96A\n" +
            "B003DNS9K4\n" +
            "B003EGIM3O\n" +
            "B003FN5JA0\n" +
            "B003FQDX0A\n" +
            "B003HD9UDA\n" +
            "B003IVMYG6\n" +
            "B003IVP1XE\n" +
            "B003IW7ZX2\n" +
            "B003KVVZZU\n" +
            "B003NDYQUG\n" +
            "B003NF068A\n" +
            "B003NJ6XK6\n" +
            "B003P9V9J4\n" +
            "B003PPETJK\n" +
            "B003TQ8RD4\n" +
            "B003U4D09G\n" +
            "B003ULNUMG\n" +
            "B003UTANVO\n" +
            "B003V8A4IQ\n" +
            "B003VN3JWY\n" +
            "B003VPX8P0\n" +
            "B003XIHVU8\n" +
            "B003ZDWFY8\n" +
            "B003ZVZPU6\n" +
            "B00403E0XG\n" +
            "B0040Q47IG\n" +
            "B004141IM0\n" +
            "B0041W63SG\n" +
            "B0042A1SEQ\n" +
            "B0042L1V10\n" +
            "B0042L3DEI\n" +
            "B0043R2QLM\n" +
            "B0047QD3CA\n" +
            "B0047V4BYO\n" +
            "B0049NQ77O\n" +
            "B0049P7622\n" +
            "B0049PRJYM\n" +
            "B0049PTMEC\n" +
            "B0049TUKGC\n" +
            "B004ASMYMK\n" +
            "B004AVRC24\n" +
            "B004CLCG8W\n" +
            "B004CRSLTE\n" +
            "B004DJ2S4K\n" +
            "B004DJATJG\n" +
            "B004DO0RLG\n" +
            "B004DZODJ2\n" +
            "B004G24H4I\n" +
            "B004GYB1RW\n" +
            "B004H1UZU8\n" +
            "B004I67HJ4\n" +
            "B004IEDMOA\n" +
            "B004INGG62\n" +
            "B004IZKHKQ\n" +
            "B004JMTY2K\n" +
            "B004JMZKNC\n" +
            "B004JNXL0A\n" +
            "B004JSMCH8\n" +
            "B004MKML8S\n" +
            "B004MKNKRE\n" +
            "B004NOIOWK\n" +
            "B004NOMVGA\n" +
            "B004NP3N7A\n" +
            "B004OWTONY\n" +
            "B004OWVN0G\n" +
            "B004PEJ05S\n" +
            "B004PEJ0DU\n" +
            "B004QF0KTQ\n" +
            "B004RBVNKY\n" +
            "B004RFGI6O\n" +
            "B004RG6W14\n" +
            "B004RR9HW4\n" +
            "B004S7BHE4\n" +
            "B004TA0JC6\n" +
            "B004VEXIJG\n" +
            "B004W50C8Y\n" +
            "B004X11QK0\n" +
            "B004X11RZ4\n" +
            "B004XM8EQI\n" +
            "B004Z54POI\n" +
            "B0050I8HI4\n" +
            "B0050IHGLI\n" +
            "B0050OB5TG\n" +
            "B0052WT0DY\n" +
            "B0052WWCCK\n" +
            "B00531AQCI\n" +
            "B00531AQK0\n" +
            "B0053BMWWU\n" +
            "B0053BMY6O\n" +
            "B0056R51QK\n" +
            "B0058J54G8\n" +
            "B005943TUK\n" +
            "B005AU6446\n" +
            "B005CTXB28\n" +
            "B005D47O9S\n" +
            "B005DMBLBC\n" +
            "B005EEQ6T6\n" +
            "B005EQ2RRE\n" +
            "B005EQ2UJE\n" +
            "B005F7RH5Y\n" +
            "B005FUXDJA\n" +
            "B005G83Y9U\n" +
            "B005HLDHUW\n" +
            "B005K9Y4IU\n" +
            "B005LGSBG8\n" +
            "B005LMRFMS\n" +
            "B005MSNGYM\n" +
            "B005MZH4DY\n" +
            "B005N9GQV0\n" +
            "B005OQEK9W\n" +
            "B005OTY3O6\n" +
            "B005OUW3HO\n" +
            "B005QKGF08\n" +
            "B005R2Q6K4\n" +
            "B005S2YYYS\n" +
            "B005S3TCQC\n" +
            "B005TVL65I\n" +
            "B005UN6NCQ\n" +
            "B005VZ9D3O\n" +
            "B005W24YWG\n" +
            "B005WGLPBK\n" +
            "B005WHG6EU\n" +
            "B005X7077G\n" +
            "B005XJ46IU\n" +
            "B005XJ4IJ2\n" +
            "B005XJ4OKK\n" +
            "B005ZJV0PU\n" +
            "B005ZMLKFW\n" +
            "B00600YKKK\n" +
            "B00600YMDK\n" +
            "B00601JWY8\n" +
            "B00639PZK2\n" +
            "B0063CF80G\n" +
            "B0064X6SOY\n" +
            "B006BRXFVW\n" +
            "B006EFSVF6\n" +
            "B006EFSVL0\n" +
            "B006EFSVNI\n" +
            "B006J1S98Y\n" +
            "B006JW15N4\n" +
            "B006K0NQQ4\n" +
            "B006NJC35W\n" +
            "B006OV86GO\n" +
            "B006SKLLWM\n" +
            "B006T2YLOE\n" +
            "B006TYP25E\n" +
            "B006WFYI14\n" +
            "B006WFYI7S\n" +
            "B006WXBD1O\n" +
            "B006XK37WO\n" +
            "B006ZKHRDC\n" +
            "B0070TWKUW\n" +
            "B0072H91PO\n" +
            "B0074GL8C2\n" +
            "B007546LPM\n" +
            "B00773QGQU\n" +
            "B0078L8PQK\n" +
            "B007FHBT7Y\n" +
            "B007FK5BHA\n" +
            "B007I1MJ0S\n" +
            "B007J42MQA\n" +
            "B007K5RCKY\n" +
            "B007ML8CNW\n" +
            "B007NYNZN0\n" +
            "B007QRYWY0\n" +
            "B007QSGYPY\n" +
            "B007RF9O3U\n" +
            "B007RTU2UA\n" +
            "B007T2V74K\n" +
            "B007TN6UUA\n" +
            "B007UNF9U6\n" +
            "B007UNFA1O\n" +
            "B007W55HPE\n" +
            "B007X2629G\n" +
            "B007X3ONV4\n" +
            "B007XQQCD8\n" +
            "B007ZZ969Y\n" +
            "B007ZZ96LC\n" +
            "B0081C02I4\n" +
            "B0081C02IO\n" +
            "B0081Z4Y1C\n" +
            "B00828JT3G\n" +
            "B00863FJW2\n" +
            "B0089V1HB8\n" +
            "B008BSZ46I\n" +
            "B008BTJCSI\n" +
            "B008BYZ2G4\n" +
            "B008CCROI4\n" +
            "B008CCTVCQ\n" +
            "B008D5G7DI\n" +
            "B008DE1GT4\n" +
            "B008EJBSS2\n" +
            "B008ES4WRW\n" +
            "B008ESP9YC\n" +
            "B008ESQCQQ\n" +
            "B008ESQJ8C\n" +
            "B008ESQOEQ\n" +
            "B008ESQSZG\n" +
            "B008ETG288\n" +
            "B008ETG2FG\n" +
            "B008ETG350\n" +
            "B008ETG3CI\n" +
            "B008EUQ00C\n" +
            "B008F7WAP8\n" +
            "B008IPUMB6\n" +
            "B008JVXB5S\n" +
            "B008M6DLBE\n" +
            "B008OT0SV0\n" +
            "B008OT0TMI\n" +
            "B008P6XCL0\n" +
            "B008P6XRGU\n" +
            "B008R5CJHM\n" +
            "B008RKJOGG\n" +
            "B008TZGFKW\n" +
            "B008U8XHB8\n" +
            "B008VWI2P4\n" +
            "B00938XDXQ\n" +
            "B0093YCQ60\n" +
            "B00974W0T4\n" +
            "B0097CYDXM\n" +
            "B009C9B83I\n" +
            "B009FV31CE\n" +
            "B009GG2RC8\n" +
            "B009GJOXRW\n" +
            "B009GJP4FW\n" +
            "B009GJPC68\n" +
            "B009H5267Y\n" +
            "B009JWDA48\n" +
            "B009M37YA0\n" +
            "B009NH6NR0\n" +
            "B009SJCP5W\n" +
            "B009SJCQ88\n" +
            "B009T5BGN2\n" +
            "B009VGRVDI\n" +
            "B009VGRVPQ\n" +
            "B009XBGF2E\n" +
            "B009YDOAGO\n" +
            "B009YWYNYO\n" +
            "B009ZCJNCU\n" +
            "B009ZF8Y18\n" +
            "B00A16686S\n" +
            "B00A39MFYW\n" +
            "B00A5TE86I\n" +
            "B00A7AFR5G\n" +
            "B00A7B0BZG\n" +
            "B00A8U85GI\n" +
            "B00A9274T4\n" +
            "B00ABH01GA\n" +
            "B00AC51OLC\n" +
            "B00ADL0M5Y\n" +
            "B00AE2OW6W\n" +
            "B00AE8DHPS\n" +
            "B00AGHXR52\n" +
            "B00AMDDR00\n" +
            "B00ANFIZCW\n" +
            "B00ANUKW8C\n" +
            "B00AOEPEDA\n" +
            "B00ARFMEZC\n" +
            "B00ARHQ2P8\n" +
            "B00ASH465U\n" +
            "B00ATZU9HA\n" +
            "B00AU012IO\n" +
            "B00AWJKWS4\n" +
            "B00AWUDGZO\n" +
            "B00AZFJ484\n" +
            "B00B0W6POM\n" +
            "B00B18KADW\n" +
            "B00B18MDL4\n" +
            "B00B1C656Y\n" +
            "B00B2KMANC\n" +
            "B00B2KMF78\n" +
            "B00B30E1VK\n" +
            "B00B5DBV9U\n" +
            "B00B5TLX2O\n" +
            "B00B6VDHL6\n" +
            "B00B6VN46Y\n" +
            "B00B6VNEGY\n" +
            "B00B7FCRGC\n" +
            "B00B8TZFZW\n" +
            "B00BBA645I\n" +
            "B00BBVD57M\n" +
            "B00BBVD6VM\n" +
            "B00BCFU9E4\n" +
            "B00BD01RVC\n" +
            "B00BEF0FQ4\n" +
            "B00BEF0MD0\n" +
            "B00BESFFO8\n" +
            "B00BFXWOP0\n" +
            "B00BGP91O4\n" +
            "B00BHXYUDM\n" +
            "B00BLZXMDU\n" +
            "B00BLZXOVU\n" +
            "B00BMHSQWE\n" +
            "B00BNRR8LI\n" +
            "B00BPWJ5QW\n" +
            "B00BQTMIMC\n" +
            "B00BQTMING\n" +
            "B00BQYPT1O\n" +
            "B00BSVO556\n" +
            "B00BTPSBBK\n" +
            "B00BXQPUNM\n" +
            "B00BYTVSFC\n" +
            "B00C0UOORI\n" +
            "B00C2RD4SO\n" +
            "B00C39YZKC\n" +
            "B00CBV449O\n" +
            "B00CCKN8FA\n" +
            "B00CDFNJ26\n" +
            "B00CDPSVQK\n" +
            "B00CDPSWQY\n" +
            "B00CDPSXEK\n" +
            "B00CE2WLUY\n" +
            "B00CE2WQK4\n" +
            "B00CE2WQN6\n" +
            "B00CG0XEP0\n" +
            "B00CGRSU9I\n" +
            "B00CK9ZW3Y\n" +
            "B00CKKQ1SS\n" +
            "B00CL8BKU8\n" +
            "B00CM9FK02\n" +
            "B00CMRAVO4\n" +
            "B00CMRR1L0\n" +
            "B00CQWLUTK\n" +
            "B00CSQEEPQ\n" +
            "B00CTYIN36\n" +
            "B00CWVOAE2\n" +
            "B00CYLNBE0\n" +
            "B00CYQ21B4\n" +
            "B00D1UL2I0\n" +
            "B00D1V4SJO\n" +
            "B00D1V5KI2\n" +
            "B00D3IYRBE\n" +
            "B00D451AMA\n" +
            "B00D6G1OCS\n" +
            "B00D7C9EQO\n" +
            "B00D840RHQ\n" +
            "B00DC6LFYO\n" +
            "B00DCUXV38\n" +
            "B00DI3KFBK\n" +
            "B00DI90758\n" +
            "B00DP47VL4\n" +
            "B00DSIEM1Y\n" +
            "B00DU8ULNU\n" +
            "B00DVH9UJ6\n" +
            "B00DZNJ14A\n" +
            "B00E15ZJJ2\n" +
            "B00E4N2PRK\n" +
            "B00E4N2Q0Q\n" +
            "B00E5E66XC\n" +
            "B00E9YJ32Y\n" +
            "B00EC7MYUQ\n" +
            "B00ECWOYZY\n" +
            "B00ECWP0RU\n" +
            "B00EE13260\n" +
            "B00EE134KO\n" +
            "B00EE13P7Q\n" +
            "B00EHJISAO\n" +
            "B00EIUMTZM\n" +
            "B00ENW423C\n" +
            "B00EO1ISKK\n" +
            "B00EOLMX2E\n" +
            "B00EOLN47C\n" +
            "B00EOSNZL0\n" +
            "B00EPBTIP8\n" +
            "B00EPE5TFI\n" +
            "B00EPE5TFS\n" +
            "B00EPG78F0\n" +
            "B00ESZEDQA\n" +
            "B00ESZEI0Q\n" +
            "B00EW0DIN0\n" +
            "B00EZML6HU\n" +
            "B00EZN8S02\n" +
            "B00F1X34DQ\n" +
            "B00F438OX8\n" +
            "B00F54I29M\n" +
            "B00F9AG6BS\n" +
            "B00F9AGIXO\n" +
            "B00FAM3T8S\n" +
            "B00FB618C2\n" +
            "B00FBE0AHI\n" +
            "B00FEANTKE\n" +
            "B00FEIAOVI\n" +
            "B00FG288N8\n" +
            "B00FGRYIXM\n" +
            "B00FGRZ1FG\n" +
            "B00FKA6JY6\n" +
            "B00FKK78GE\n" +
            "B00FPCWLCS\n" +
            "B00FPCWLLE\n" +
            "B00FQIC3AQ\n" +
            "B00FS1OQXI\n" +
            "B00FWR0TZC\n" +
            "B00FXO8WFI\n" +
            "B00FYVS0WU\n" +
            "B00FZM5WEM\n" +
            "B00G3TVB32\n" +
            "B00G4A4WM2\n" +
            "B00G5SE0V6\n" +
            "B00G6HINVO\n" +
            "B00G6YJU6Y\n" +
            "B00G8ZR1LM\n" +
            "B00G92EWRK\n" +
            "B00GA2HQAO\n" +
            "B00GAS9BVA\n" +
            "B00GAZZIMY\n" +
            "B00GB98SXA\n" +
            "B00GGX5DWU\n" +
            "B00GHDEMA8\n" +
            "B00GHEM3IK\n" +
            "B00GHFRKSW\n" +
            "B00GMV5HDG\n" +
            "B00GNVDFCU\n" +
            "B00GRRKV7C\n" +
            "B00GT5QC98\n" +
            "B00GTNCTTC\n" +
            "B00GV9PODC\n" +
            "B00GV9TQAO\n" +
            "B00GVE4HQ2\n" +
            "B00GXNEDME\n" +
            "B00GZEVPE0\n" +
            "B00H1OR2IG\n" +
            "B00H2PXKWQ\n" +
            "B00H3W4QF8\n" +
            "B00H3W4QJO\n" +
            "B00H4927ZQ\n" +
            "B00H4UNGIW\n" +
            "B00HA0FBA2\n" +
            "B00HCMYEI8\n" +
            "B00HCNM1JG\n" +
            "B00HCNP0BC\n" +
            "B00HDKREF4\n" +
            "B00HF2X9EA\n" +
            "B00HF2XBXY\n" +
            "B00HHCV1RU\n" +
            "B00HNQ839O\n" +
            "B00HNV1CEM\n" +
            "B00HRU52KY\n" +
            "B00HT94VL4\n" +
            "B00HUZGL3S\n" +
            "B00HWCZQGC\n" +
            "B00HWNHYUM\n" +
            "B00HWS8VRC\n" +
            "B00HWS9AL8\n" +
            "B00HX86IY4\n" +
            "B00HXHXGGS\n" +
            "B00HXIGGUU\n" +
            "B00HYL6MIC\n" +
            "B00HYZIPX8\n" +
            "B00HZ2MSLK\n" +
            "B00HZRCRRA\n" +
            "B00I10RHDO\n" +
            "B00I2HTPLS\n" +
            "B00I3C2JQ0\n" +
            "B00I4733F0\n" +
            "B00I5L4UL6\n" +
            "B00I5L4UOI\n" +
            "B00I5P9ZW6\n" +
            "B00I93Z8T8\n" +
            "B00I96MB22\n" +
            "B00I9XYFJ2\n" +
            "B00IAOB4VC\n" +
            "B00IAPEHMO\n" +
            "B00IAPOUG2\n" +
            "B00IAQZNBM\n" +
            "B00IH21URK\n" +
            "B00IIJZUJ6\n" +
            "B00IIOZ0XC\n" +
            "B00IL51T4C\n" +
            "B00IMFWHYW\n" +
            "B00IOR2A2M\n" +
            "B00IPII7R2\n" +
            "B00IRZIWVY\n" +
            "B00ISX016E\n" +
            "B00IVF04FC\n" +
            "B00IVF04LG\n" +
            "B00IZ8T45G\n" +
            "B00J2C5RFA\n" +
            "B00J9G1MIK\n" +
            "B00JA0RLFI\n" +
            "B00JAZ987M\n" +
            "B00JB7ORUM\n" +
            "B00JD4TFDW\n" +
            "B00JI8UJN8\n" +
            "B00JLD4M3I\n" +
            "B00JLSTR2Y\n" +
            "B00JS13B1W\n" +
            "B00JSOHZTS\n" +
            "B00JY2H83M\n" +
            "B00K1DGPQE\n" +
            "B00K2WAWE0\n" +
            "B00K5R7GSW\n" +
            "B00K67RFCS\n" +
            "B00K7XZMIU\n" +
            "B00KAFRUQM\n" +
            "B00KCLURPA\n" +
            "B00KCM7RK2\n" +
            "B00KDE2YH0\n" +
            "B00KJIW5V0\n" +
            "B00KL8K3V2\n" +
            "B00KM3USAC\n" +
            "B00KM3UTCY\n" +
            "B00KQJPW8K\n" +
            "B00KTB9J48\n" +
            "B00KTDRMO0\n" +
            "B00KTF2H5W\n" +
            "B00KU2O2R0\n" +
            "B00KW15CEG\n" +
            "B00L1RZT1G\n" +
            "B00L28DKO2\n" +
            "B00L37AZ90\n" +
            "B00L37AZZY\n" +
            "B00L46G2H4\n" +
            "B00L47FTPY\n" +
            "B00L4BL0JE\n" +
            "B00L5GL4EO\n" +
            "B00L7CMAUS\n" +
            "B00L7MR2K6\n" +
            "B00L8ZJKUW\n" +
            "B00LGNS980\n" +
            "B00LGNSHDC\n" +
            "B00LHLC62G\n" +
            "B00LHXZFPO\n" +
            "B00LIYYJEK\n" +
            "B00LJ0WPJO\n" +
            "B00LM9UFE4\n" +
            "B00LNC2PL6\n" +
            "B00LO7VWBO\n" +
            "B00LO9XYM2\n" +
            "B00LO9Y45S\n" +
            "B00LOYF89E\n" +
            "B00LUJE97A\n" +
            "B00LVKGLWY\n" +
            "B00LZ2OV2A\n" +
            "B00LZNNFCG\n" +
            "B00M0OFEJ6\n" +
            "B00M0P0L7A\n" +
            "B00M2IE3Z6\n" +
            "B00M5ANY24\n" +
            "B00M93X3DC\n" +
            "B00MA3TH2M\n" +
            "B00MB6K86W\n" +
            "B00MFHQT60\n" +
            "B00MG1C0U4\n" +
            "B00MJ4NFVG\n" +
            "B00MNBPY0K\n" +
            "B00MNBQ742\n" +
            "B00MPELKJ4\n" +
            "B00MX1U4ZA\n" +
            "B00MX58NT0\n" +
            "B00N1RC5FM\n" +
            "B00N2TAY8E\n" +
            "B00N4OX1J6\n" +
            "B00N8YZ7CG\n" +
            "B00N9ZXT10\n" +
            "B00NA054EE\n" +
            "B00NA1NBQG\n" +
            "B00NAQG97E\n" +
            "B00NAVPK7O\n" +
            "B00NGOP93U\n" +
            "B00NGTLYQQ\n" +
            "B00NGTRO24\n" +
            "B00NGYKXNQ\n" +
            "B00NH9WF4K\n" +
            "B00NIWSSXS\n" +
            "B00NIXB942\n" +
            "B00NL8M5DS\n" +
            "B00NM8Q1R8\n" +
            "B00NN6RTGQ\n" +
            "B00NOZPBBQ\n" +
            "B00NP33RYU\n" +
            "B00NPWCFKI\n" +
            "B00NQA34AE\n" +
            "B00NSW4AXA\n" +
            "B00NXYCBBG\n" +
            "B00O2XN9U4\n" +
            "B00O443QK4\n" +
            "B00O45ICZ2\n" +
            "B00O57B5AI\n" +
            "B00O82D2X8\n" +
            "B00O8TCPMK\n" +
            "B00O9YOAGS\n" +
            "B00OACVZHG\n" +
            "B00OB5G8CO\n" +
            "B00OB7PXMS\n" +
            "B00OBU0D20\n" +
            "B00ODZ4XEC\n" +
            "B00OH46TSW\n" +
            "B00OJECW7C\n" +
            "B00OK4P6LA\n" +
            "B00OKU152I\n" +
            "B00OS6RLDQ\n" +
            "B00OU032EC\n" +
            "B00OUV4UKG\n" +
            "B00OXV8WB6\n" +
            "B00OZLSWYQ\n" +
            "B00OZN01RA\n" +
            "B00P0JZYHU\n" +
            "B00P0K0O6A\n" +
            "B00P2KU0BC\n" +
            "B00PAU7RCO\n" +
            "B00PJQGSVA\n" +
            "B00PLVOYO6\n" +
            "B00PQTGTP0\n" +
            "B00PXUUFA2\n" +
            "B00Q1R862A\n" +
            "B00Q5JZ0B4\n" +
            "B00QKIF01A\n" +
            "B00QL1Z2PK\n" +
            "B00QL1Z3RC\n" +
            "B00QL1ZD4A\n" +
            "B00QTVHGQK\n" +
            "B00QU4GT7I\n" +
            "B00QV8FXZW\n" +
            "B00R36BB20\n" +
            "B00R61Y084\n" +
            "B00R65QSYE\n" +
            "B00R9HOINW\n" +
            "B00R9HOIXW\n" +
            "B00RBHUE7E\n" +
            "B00RBHUMX0\n" +
            "B00RBHVKFE\n" +
            "B00REMT2C4\n" +
            "B00RKBOQ02\n" +
            "B00RLOQTW6\n" +
            "B00RLOQVAQ\n" +
            "B00RXG928U\n" +
            "B00S0A6CPY\n" +
            "B00S556VDC\n" +
            "B00SA6L3O8\n" +
            "B00SCTJDR2\n" +
            "B00SCTJG9M\n" +
            "B00SFY04JA\n" +
            "B00SH41QM2\n" +
            "B00SHODYJK\n" +
            "B00SOTG05S\n" +
            "B00SQZTKTI\n" +
            "B00SXEYD6C\n" +
            "B00T06IF16\n" +
            "B00T06INIG\n" +
            "B00T06IPUC\n" +
            "B00T41USLC\n" +
            "B00T4LH2YI\n" +
            "B00T7L4LUS\n" +
            "B00T9VVHU8\n" +
            "B00TANPR0G\n" +
            "B00TBA13P6\n" +
            "B00TDQV10A\n" +
            "B00TDQV10U\n" +
            "B00TECTI0S\n" +
            "B00TGR18FO\n" +
            "B00TICBXPW\n" +
            "B00TKB9DEO\n" +
            "B00TLTOEW6\n" +
            "B00TOK58UO\n" +
            "B00TOLDN0A\n" +
            "B00TUS5RPQ\n" +
            "B00TWIMT96\n" +
            "B00TYF7HK8\n" +
            "B00U2EAA36\n" +
            "B00U2ILPOU\n" +
            "B00U79QCR4\n" +
            "B00U7CAE2A\n" +
            "B00U7CB2BM\n" +
            "B00U7GP26Y\n" +
            "B00U7LMX3Y\n" +
            "B00U8ACRO4\n" +
            "B00U8S1LLG\n" +
            "B00U8S1LR0\n" +
            "B00UALJO9M\n" +
            "B00UJS4Q6M\n" +
            "B00UMTAJVE\n" +
            "B00UMTAK40\n" +
            "B00URCXFG2\n" +
            "B00UTDVZ5W\n" +
            "B00UTIAULM\n" +
            "B00V6D7CPG\n" +
            "B00V6DXAZW\n" +
            "B00V9HIPKK\n" +
            "B00VA5HRVY\n" +
            "B00VA5I1NM\n" +
            "B00VA5IATM\n" +
            "B00VFKCQZQ\n" +
            "B00VIVLG6W\n" +
            "B00VRPQYFW\n" +
            "B00VX2QCBU\n" +
            "B00W2RPATA\n" +
            "B00W3GMJKI\n" +
            "B00W40ZHS4\n" +
            "B00W6EAVUC\n" +
            "B00W6ZVXGW\n" +
            "B00W703FO4\n" +
            "B00W79X7NE\n" +
            "B00W79X7PM\n" +
            "B00W7KRWUC\n" +
            "B00W9X816Q\n" +
            "B00WAS3XTK\n" +
            "B00WFM71KI\n" +
            "B00WFV87BQ\n" +
            "B00WGI3O8E\n" +
            "B00WIOG0G4\n" +
            "B00WJIFPBA\n" +
            "B00WMRH47Q\n" +
            "B00WOJPHN0\n" +
            "B00WOJPM3A\n" +
            "B00WOJPWG2\n" +
            "B00WS6CX5E\n" +
            "B00WUZI4KQ\n" +
            "B00X53MAIE\n" +
            "B00X9VY1BQ\n" +
            "B00XEM4P84\n" +
            "B00XHPXQGK\n" +
            "B00XHR6BUG\n" +
            "B00XHSPBZQ\n" +
            "B00XI41TKK\n" +
            "B00XW63PKQ\n" +
            "B00XWAVS6U\n" +
            "B00XYFWH2M\n" +
            "B00XYO1J04\n" +
            "B00Y32R0RW\n" +
            "B00Y7RGHWM\n" +
            "B00Y9U8IU6\n" +
            "B00YAKJB74\n" +
            "B00YCATDXY\n" +
            "B00YKSUCZG\n" +
            "B00YUAGVC2\n" +
            "B00ZA7QXR2\n" +
            "B00ZG00R16\n" +
            "B00ZK2QMAA\n" +
            "B00ZK3QEI4\n" +
            "B00ZLYTJXE\n" +
            "B00ZPL6ZGW\n" +
            "B00ZSRXK1Q\n" +
            "B00ZX51OW0\n" +
            "B0100BX2WQ\n" +
            "B0107WGWDE\n" +
            "B010BJUD7O\n" +
            "B010D07T0A\n" +
            "B010D08AVW\n" +
            "B010FW3MOI\n" +
            "B010LN8SHC\n" +
            "B010LOMGXS\n" +
            "B010LY8O0M\n" +
            "B010NAPQPA\n" +
            "B010SS04YK\n" +
            "B010U3VC62\n" +
            "B010V78Z9O\n" +
            "B01170NOEK\n" +
            "B011QQ2VLM\n" +
            "B011TWZUPC\n" +
            "B0122XH28K\n" +
            "B0123D922S\n" +
            "B0124HJRBE\n" +
            "B0128V0260\n" +
            "B012E4V1P2\n" +
            "B012ENUGOK\n" +
            "B012ESDUMA\n" +
            "B012HJI7BA\n" +
            "B012K1TRUK\n" +
            "B012S11YDA\n" +
            "B012W0SCGY\n" +
            "B012W0UQB8\n" +
            "B0132QPJR2\n" +
            "B0132QQGNS\n" +
            "B0136VZIJC\n" +
            "B013B7G6YM\n" +
            "B013CRJJDG\n" +
            "B013E40UG2\n" +
            "B013E8MGW4\n" +
            "B013GPMLJI\n" +
            "B013M9ZGTK\n" +
            "B013MA2IYA\n" +
            "B013QSKY4E\n" +
            "B013UZDCD8\n" +
            "B013WAV8W8\n" +
            "B013WB1PA2\n" +
            "B013WIS3I2\n" +
            "B0142H8AG2\n" +
            "B0144YJ6C0\n" +
            "B0145SFYZS\n" +
            "B0148DSPWE\n" +
            "B0148T39CY\n" +
            "B0148VPS4O\n" +
            "B014FB0VCG\n" +
            "B014QB3762\n" +
            "B014RTUF9K\n" +
            "B014W2BIIO\n" +
            "B014W9R458\n" +
            "B0150C4BZM\n" +
            "B0150C4C6K\n" +
            "B01518MR1U\n" +
            "B0153NIJYW\n" +
            "B0155YQGE4\n" +
            "B015EAJU56\n" +
            "B015HLXNDM\n" +
            "B015IUVH0S\n" +
            "B015P8JFRK\n" +
            "B015PUJJAG\n" +
            "B015PUJYDS\n" +
            "B015PUL3VO\n" +
            "B015RECW6I\n" +
            "B015UWZ1YW\n" +
            "B015VNXBEC\n" +
            "B015W7S9BW\n" +
            "B015XOPJR6\n" +
            "B015YFFQUE\n" +
            "B015ZMZE30\n" +
            "B01619B6TM\n" +
            "B0163LZSVU\n" +
            "B0167J2FUU\n" +
            "B0169H6Y5M\n" +
            "B0169RPJ0S\n" +
            "B016AH9TAS\n" +
            "B016AWPFME\n" +
            "B016AWPL00\n" +
            "B016DIR38O\n" +
            "B016E2JW6K\n" +
            "B016EWPDYA\n" +
            "B016ICRJMU\n" +
            "B016LG0WA4\n" +
            "B016QOF5SK\n" +
            "B016QPYJ2C\n" +
            "B016V1ZSGC\n" +
            "B016X4TMB4\n" +
            "B016X4TN86\n" +
            "B017A84406\n" +
            "B017CI5D18\n" +
            "B017D2JA6W\n" +
            "B017IR4BJ8\n" +
            "B017IR4DDC\n" +
            "B017JN4T0M\n" +
            "B017JS01H2\n" +
            "B017NKW80O\n" +
            "B01846Y90I\n" +
            "B0184CITDU\n" +
            "B01891LYZQ\n" +
            "B01891LZBY\n" +
            "B0189OKC0G\n" +
            "B018AR1ADA\n" +
            "B018E0L0PG\n" +
            "B018F0A5YW\n" +
            "B018IF7XAS\n" +
            "B018WLE2OS\n" +
            "B018WLZZZI\n" +
            "B018XKQOWQ\n" +
            "B01956RV2O\n" +
            "B0196E1T7S\n" +
            "B01985IX9M\n" +
            "B0198TBNM2\n" +
            "B0199SXJ60\n" +
            "B019C9P56S\n" +
            "B019CAYZAO\n" +
            "B019CKXHDK\n" +
            "B019CKXJ7E\n" +
            "B019EVY5VK\n" +
            "B019EVY61E\n" +
            "B019FELD48\n" +
            "B019N6XV3O\n" +
            "B019NZ86M6\n" +
            "B019OSG318\n" +
            "B019X3EY7O\n" +
            "B01A0SBJV0\n" +
            "B01A7ZN374\n" +
            "B01A7ZN92I\n" +
            "B01A7ZUSZO\n" +
            "B01AHR5ZFA\n" +
            "B01AI2PL3K\n" +
            "B01AI2PSR4\n" +
            "B01AI58H7Y\n" +
            "B01AK0E6CC\n" +
            "B01AK0FILK\n" +
            "B01AK0HH1E\n" +
            "B01AMK0NL8\n" +
            "B01AUN5VI2\n" +
            "B01AY0VDKG\n" +
            "B01B0JPPB8\n" +
            "B01B1PT66Q\n" +
            "B01BATEJ7Y\n" +
            "B01BBZD9E6\n" +
            "B01BD87O38\n" +
            "B01BDNX4UK\n" +
            "B01BDRRSXK\n" +
            "B01BGQFYB6\n" +
            "B01BI8CP8C\n" +
            "B01BNDNEGO\n" +
            "B01BNIPNZY\n" +
            "B01BNMSSXO\n" +
            "B01BOVWEFC\n" +
            "B01BR023KG\n" +
            "B01BTM0ZTI\n" +
            "B01BUEGC9W\n" +
            "B01C5MXRUU\n" +
            "B01C8K9VWC\n" +
            "B01C8K9XVQ\n" +
            "B01CBFAE7U\n" +
            "B01CCVHSFY\n" +
            "B01CDGUDW8\n" +
            "B01CET05B8\n" +
            "B01CFPLY3O\n" +
            "B01CHRHYL6\n" +
            "B01CHUZI84\n" +
            "B01CHVXNXA\n" +
            "B01CI29KMQ\n" +
            "B01CI5U28E\n" +
            "B01CNW80AY\n" +
            "B01CSGKEJU\n" +
            "B01CSZFETG\n" +
            "B01CT03BIG\n" +
            "B01CTWGXNY\n" +
            "B01CVRXXOO\n" +
            "B01D7OTJ2K\n" +
            "B01D8ZLM32\n" +
            "B01D9OBDOU\n" +
            "B01DPPWZ2C\n" +
            "B01DS3F6UE\n" +
            "B01DUGTCPE\n" +
            "B01DWLZ65W\n" +
            "B01DY51DMG\n" +
            "B01E3P74FG\n" +
            "B01E3YAB5M\n" +
            "B01E3YACSS\n" +
            "B01E3YAQP2\n" +
            "B01E42GUYY\n" +
            "B01E4JO88M\n" +
            "B01E4K55UQ\n" +
            "B01E56UDWO\n" +
            "B01E5EW65I\n" +
            "B01E5IRFBO\n" +
            "B01EA3E46S\n" +
            "B01EAE0LU0\n" +
            "B01EAEJXN6\n" +
            "B01EC50AIK\n" +
            "B01ECAK47W\n" +
            "B01ECKGNMM\n" +
            "B01EJR6MGK\n" +
            "B01EM1NQ9Y\n" +
            "B01EO45SDG\n" +
            "B01ERDIBUG\n" +
            "B01EUELAUK\n" +
            "B01EVDZQ8W\n" +
            "B01EY7LRIS\n" +
            "B01EYWNUPQ\n" +
            "B01EZ8TY54\n" +
            "B01EZ8TZNK\n" +
            "B01F23547S\n" +
            "B01F3FR1YO\n" +
            "B01F6ZEU2C\n" +
            "B01F6ZEU2W\n" +
            "B01F8Y5P2A\n" +
            "B01FHQ5D2G\n" +
            "B01FNJ8Z72\n" +
            "B01FO0096A\n" +
            "B01FRA6JRK\n" +
            "B01FSO1KUQ\n" +
            "B01FU34QXI\n" +
            "B01FUFYEW4\n" +
            "B01G1BD32S\n" +
            "B01G3H25ME\n" +
            "B01G3N2WP8\n" +
            "B01G4907QM\n" +
            "B01G55LDAO\n" +
            "B01G74GZQU\n" +
            "B01GCI6YF8\n" +
            "B01GCI6YJO\n" +
            "B01GHWEEHE\n" +
            "B01GT1JBZS\n" +
            "B01GXFUJKQ\n" +
            "B01H3P10X0\n" +
            "B01H5E4U8G\n" +
            "B01H840NV6\n" +
            "B01HBG2PQ2\n" +
            "B01HFOFSEG\n" +
            "B01HI06T5Y\n" +
            "B01HQB5AYG\n" +
            "B01HSWEVSE\n" +
            "B01HUVNH7O\n" +
            "B01I0ZIXSC\n" +
            "B01I1FJSIU\n" +
            "B01I1NWLHW\n" +
            "B01I2XDPMG\n" +
            "B01I37ALF0\n" +
            "B01I6GZH66\n" +
            "B01IBQMQFG\n" +
            "B01IETPK76\n" +
            "B01IN8LBS0\n" +
            "B01IP2O9OW\n" +
            "B01IRD2M7A\n" +
            "B01ISL8AEK\n" +
            "B01ITDYHA8\n" +
            "B01IV8ZX0E\n" +
            "B01IY1NHGA\n" +
            "B01J5FG928\n" +
            "B01J5FG9BE\n" +
            "B01JA5HPTY\n" +
            "B01JA5HX2I\n" +
            "B01JOXY26Q\n" +
            "B01JP4403E\n" +
            "B01JP4409I\n" +
            "B01JSBE9MQ\n" +
            "B01KHDG3WS\n" +
            "B01KOJFS9E\n" +
            "B01KOJFV0A\n" +
            "B01KSXMBLY\n" +
            "B01KUQIJ6U\n" +
            "B01KUU61VQ\n" +
            "B01KX9N6R6\n" +
            "B01KXYOT90\n" +
            "B01KZTSPZM\n" +
            "B01KZU2P5W\n" +
            "B01L7PPH40\n" +
            "B01LBC669E\n" +
            "B01LEPNEDE\n" +
            "B01LGSOAQ4\n" +
            "B01LMT18PS\n" +
            "B01LPOLDWS\n" +
            "B01LS4CFDG\n" +
            "B01LS99M6E\n" +
            "B01LVXFQTB\n" +
            "B01LW46HZ0\n" +
            "B01LWLE3ZW\n" +
            "B01LX610QN\n" +
            "B01LX9F0N3\n" +
            "B01LXB3IS5\n" +
            "B01LXHOMR5\n" +
            "B01LY219UD\n" +
            "B01LY2YORD\n" +
            "B01LY84EO4\n" +
            "B01LYHUJCA\n" +
            "B01M0W3YNJ\n" +
            "B01M1CFKTI\n" +
            "B01M26LRXH\n" +
            "B01M3PG1CT\n" +
            "B01M3PKO5Q\n" +
            "B01M3YHJLR\n" +
            "B01M4LSMX9\n" +
            "B01M5AZW0F\n" +
            "B01M5B4I86\n" +
            "B01M8K06AT\n" +
            "B01M9AMCH7\n" +
            "B01M9JSEI9\n" +
            "B01MAXT3YA\n" +
            "B01MAZAIPF\n" +
            "B01MCUOXF8\n" +
            "B01MCY9LNI\n" +
            "B01MDJ5YJD\n" +
            "B01MDJAXAR\n" +
            "B01MDLKYPX\n" +
            "B01MDMZGJO\n" +
            "B01MDR0R69\n" +
            "B01MDSIX1N\n" +
            "B01MEGKVT7\n" +
            "B01MFCH13P\n" +
            "B01MFG5U2R\n" +
            "B01MFGH8B7\n" +
            "B01MPXL3LN\n" +
            "B01MQVQUEB\n" +
            "B01MRJO5JX\n" +
            "B01MSD97J5\n" +
            "B01MSM1OJC\n" +
            "B01MT0SOZV\n" +
            "B01MT82XBU\n" +
            "B01MTB1I5T\n" +
            "B01MTIVOSO\n" +
            "B01MTJAWP5\n" +
            "B01MTKAZHV\n" +
            "B01MTP2E7U\n" +
            "B01MTQTOEJ\n" +
            "B01MTUYV4P\n" +
            "B01MTYGLBI\n" +
            "B01MU2ZW57\n" +
            "B01MU39KJK\n" +
            "B01MU4F63M\n" +
            "B01MU886GO\n" +
            "B01MUA4L5M\n" +
            "B01MUGD6HO\n" +
            "B01MUGXRT9\n" +
            "B01MUHILO5\n" +
            "B01MUHIN3I\n" +
            "B01MXEDG3A\n" +
            "B01MXJPP75\n" +
            "B01MXPK8IG\n" +
            "B01MY6IKF1\n" +
            "B01MY7W7SP\n" +
            "B01MYBKOH8\n" +
            "B01MYEBXTW\n" +
            "B01MZ8DOO1\n" +
            "B01MZIWF00\n" +
            "B01N0545DA\n" +
            "B01N0AXT9K\n" +
            "B01N0BDWLP\n" +
            "B01N0E99EX\n" +
            "B01N0RIIWL\n" +
            "B01N0SV92Z\n" +
            "B01N0ULHY4\n" +
            "B01N199FR3\n" +
            "B01N1ESO3I\n" +
            "B01N21TM9V\n" +
            "B01N27IIZ3\n" +
            "B01N2990FJ\n" +
            "B01N2H3PNB\n" +
            "B01N2PJJCT\n" +
            "B01N2TETCD\n" +
            "B01N30HQ7W\n" +
            "B01N34VAV6\n" +
            "B01N3OWLMV\n" +
            "B01N47FVNN\n" +
            "B01N4B9SOE\n" +
            "B01N5HZX9T\n" +
            "B01N64SZN7\n" +
            "B01N6DZS2N\n" +
            "B01N760VXD\n" +
            "B01N7K7PH4\n" +
            "B01N7R5REX\n" +
            "B01N9LPMO9\n" +
            "B01N9LQR36\n" +
            "B01N9RFZ5Y\n" +
            "B01N9VYWH1\n" +
            "B01NARG5UP\n" +
            "B01NB0ZIEG\n" +
            "B01NBB99UG\n" +
            "B01NBDBFS2\n" +
            "B01NBQXO74\n" +
            "B01NCETFNU\n" +
            "B01NCLLRIF\n" +
            "B01NCWVYWX\n" +
            "B01NGTI93E\n" +
            "B01NH7JSTY\n" +
            "B06VT1G3B9\n" +
            "B06VWT5T5T\n" +
            "B06VWW8LX7\n" +
            "B06VXND65B\n" +
            "B06W57SJGG\n" +
            "B06W9FF9W2\n" +
            "B06WD5V9PK\n" +
            "B06WD8NRL2\n" +
            "B06WVNZFQW\n" +
            "B06WW624XY\n" +
            "B06X997WDQ\n" +
            "B06X9B5C7H\n" +
            "B06X9TMLC5\n" +
            "B06XC8DRQT\n" +
            "B06XCF3SB8\n" +
            "B06XCFZG84\n" +
            "B06XCHRM73\n" +
            "B06XCYC29H\n" +
            "B06XCZMCLR\n" +
            "B06XDB9574\n" +
            "B06XDBY8Y7\n" +
            "B06XDN6WHD\n" +
            "B06XFRFLQK\n" +
            "B06XFX6Q8Y\n" +
            "B06XGDZRF9\n" +
            "B06XGNWJ4H\n" +
            "B06XKCDX8L\n" +
            "B06XNQXT2L\n" +
            "B06XP5ZWCP\n" +
            "B06XPDH2WF\n" +
            "B06XPZ1CRF\n" +
            "B06XQ4F9GW\n" +
            "B06XR4N4JZ\n" +
            "B06XRF5R62\n" +
            "B06XRFGP1L\n" +
            "B06XRFSH2D\n" +
            "B06XRPZFY9\n" +
            "B06XSV7H9W\n" +
            "B06XT1P9H6\n" +
            "B06XTHCZX7\n" +
            "B06XTTDXYJ\n" +
            "B06XVCWKPG\n" +
            "B06XVWR7SD\n" +
            "B06XW11SFT\n" +
            "B06XWVMZ8G\n" +
            "B06XXL1MHN\n" +
            "B06Y1P6DPZ\n" +
            "B06Y1Z3BWF\n" +
            "B06Y274XD6\n" +
            "B06Y2BK9L1\n" +
            "B06Y2T36C9\n" +
            "B06Y5KSZ2K\n" +
            "B06Y5L9JFW\n" +
            "B06Y5PTBB9\n" +
            "B06Y5WN1NW\n" +
            "B06Y6928CY\n" +
            "B06Y6FLZ6M\n" +
            "B06Y6KYQ9N\n" +
            "B06Y6MBZY4\n" +
            "B06ZZNF3FN\n" +
            "B07111GFF1\n" +
            "B0711RLW7N\n" +
            "B0713XR7R5\n" +
            "B0718W4YSR\n" +
            "B071988ZGL\n" +
            "B0719CH76D\n" +
            "B0719MK68L\n" +
            "B0719V9275\n" +
            "B071DM6K8W\n" +
            "B071DPXJF3\n" +
            "B071FFDN58\n" +
            "B071FT63LD\n" +
            "B071GMZP6V\n" +
            "B071HX8NSY\n" +
            "B071K5FQYN\n" +
            "B071K6D4Z2\n" +
            "B071KN1442\n" +
            "B071L1KH29\n" +
            "B071LQFMXH\n" +
            "B071PDLN8L\n" +
            "B071RJDHFM\n" +
            "B071RWF2ZZ\n" +
            "B071V4YH6H\n" +
            "B071V74BYQ\n" +
            "B071VXR2J7\n" +
            "B071WJGQ25\n" +
            "B071WNWTBZ\n" +
            "B071WRRVQQ\n" +
            "B071WWCK6F\n" +
            "B071XRW2BS\n" +
            "B071YSQJSP\n" +
            "B071Z36G5G\n" +
            "B071Z5TXFV\n" +
            "B071ZPPLKV\n" +
            "B071ZPZ9GS\n" +
            "B071ZS82G3\n" +
            "B071ZZ8774\n" +
            "B07217NG1R\n" +
            "B07219NMZK\n" +
            "B0721JWJJW\n" +
            "B0721K3JGV\n" +
            "B0722YGTY9\n" +
            "B07289DKFR\n" +
            "B072C41KZH\n" +
            "B072F1VB91\n" +
            "B072HYNQBL\n" +
            "B072J9L2C1\n" +
            "B072K35BFG\n" +
            "B072LF1P5K\n" +
            "B072LHVG1F\n" +
            "B072M3SCTT\n" +
            "B072M72TQS\n" +
            "B072PXFXT8\n" +
            "B072PYRFMR\n" +
            "B072QMJRBK\n" +
            "B072QVQSY2\n" +
            "B072R5MGPX\n" +
            "B072XDJ985\n" +
            "B072XGTN4T\n" +
            "B072XL549J\n" +
            "B07332SXPS\n" +
            "B07333DKLW\n" +
            "B07337B578\n" +
            "B0733J9FGB\n" +
            "B0734Y8WCJ\n" +
            "B073B251YT\n" +
            "B073CSNF25\n" +
            "B073F2M2MK\n" +
            "B073FLC361\n" +
            "B073HF6D42\n" +
            "B073JHBWGD\n" +
            "B073QQ98RX\n" +
            "B073RFVFF8\n" +
            "B073RLXWKB\n" +
            "B073S6MJMN\n" +
            "B073VMQCCN\n" +
            "B073VR8PXC\n" +
            "B073WX4L3J\n" +
            "B073WX6VS5\n" +
            "B073YFCPDH\n" +
            "B073Z76TVS\n" +
            "B07415PRZD\n" +
            "B0743GLWLX\n" +
            "B0743L2MXN\n" +
            "B0746CWJQ7\n" +
            "B0746GRMZL\n" +
            "B0746HBWTD\n" +
            "B0747NMQP7\n" +
            "B07484FQ2F\n" +
            "B0748SWP5W\n" +
            "B0749MCK9V\n" +
            "B0749MFF8F\n" +
            "B0749TX8MD\n" +
            "B074C4QYFV\n" +
            "B074C4XH27\n" +
            "B074F3K6XM\n" +
            "B074H2YC2Z\n" +
            "B074H34MYY\n" +
            "B074J49T97\n" +
            "B074J8TPXJ\n" +
            "B074P1QLX7\n" +
            "B074PTVKFK\n" +
            "B074QHJPV7\n" +
            "B074R7YTKQ\n" +
            "B074T7Z7PY\n" +
            "B074V6G149\n" +
            "B074VCK5W5\n" +
            "B074W61PMV\n" +
            "B074W7KGNT\n" +
            "B074WDRM2B\n" +
            "B074WDSWN4\n" +
            "B074X13R99\n" +
            "B074XD5WCN\n" +
            "B074XH1869\n" +
            "B074XK2D8W\n" +
            "B074ZS8BKM\n" +
            "B075164YQ9\n" +
            "B075193JRX\n" +
            "B0751BWJYB\n" +
            "B0751HQZ3V\n" +
            "B0754NRNYR\n" +
            "B0756GW8SJ\n" +
            "B07573H3GL\n" +
            "B075763PTS\n" +
            "B0757TZ8PS\n" +
            "B07583QWB2\n" +
            "B0759T5CYZ\n" +
            "B075C22K93\n" +
            "B075D4JN29\n" +
            "B075FVNKVZ\n" +
            "B075GSWT29\n" +
            "B075GTHRMY\n" +
            "B075GZ5NJR\n" +
            "B075JGDK4V\n" +
            "B075KHCDMT\n" +
            "B075KK797F\n" +
            "B075LFHKN8\n" +
            "B075M9PDGN\n" +
            "B075MBJ8R2\n" +
            "B075P471HR\n" +
            "B075QJXP66\n" +
            "B075QK8LB1\n" +
            "B075SDCY3F\n" +
            "B075VS6DLF\n" +
            "B075VSZ6QH\n" +
            "B075XHP668\n" +
            "B075XMPRN9\n" +
            "B075Z5HYTK\n" +
            "B075ZQQ9V7\n" +
            "B0761KLS1H\n" +
            "B0761T2G33\n" +
            "B0762HY88V\n" +
            "B0763W1739\n" +
            "B07643KNM8\n" +
            "B07652XS27\n" +
            "B07655PKX4\n" +
            "B0768K39MX\n" +
            "B0768W2ZZ4\n" +
            "B076CPSJ6M\n" +
            "B076DFF7QH\n" +
            "B076DGJYWF\n" +
            "B076DXT6SR\n" +
            "B076F2DZVS\n" +
            "B076F56PC1\n" +
            "B076F66RP4\n" +
            "B076FCFG58\n" +
            "B076HKY62P\n" +
            "B076HP5RQM\n" +
            "B076HVVCW4\n" +
            "B076HYT2MG\n" +
            "B076RPHR7W\n" +
            "B076SLDX6Q\n" +
            "B076T3XFK3\n" +
            "B076TRC1TL\n" +
            "B076XQS2JF\n" +
            "B076ZHMNKM\n" +
            "B076ZJW33G\n" +
            "B076ZSDDNV\n" +
            "B077183RCB\n" +
            "B0771CRSXH\n" +
            "B0771WVJX3\n" +
            "B0772DFQKJ\n" +
            "B0773NGL47\n" +
            "B0775R9TT3\n" +
            "B0777K628S\n" +
            "B0777S761X\n" +
            "B0777VTN22\n" +
            "B0779D54KL\n" +
            "B0779DY2FL\n" +
            "B0779FWS3W\n" +
            "B0779PYZ5P\n" +
            "B077FHCVLN\n" +
            "B077GKS52M\n" +
            "B077HTZX45\n" +
            "B077JWF8BC\n" +
            "B077JYSBWF\n" +
            "B077K4G2NK\n" +
            "B077M97L9G\n" +
            "B077P6ZCN7\n" +
            "B077P9N3JL\n" +
            "B077PPBZ9Q\n" +
            "B077Q7556Z\n" +
            "B077QDX8W7\n" +
            "B077QH2Y7S\n" +
            "B077R8FF5N\n" +
            "B077RKXV93\n" +
            "B077RYWCMP\n" +
            "B077SW6Y4G\n" +
            "B077T3CBYQ\n" +
            "B077V6SDYY\n" +
            "B077VG37R3\n" +
            "B077XY823N\n" +
            "B077YJ793G\n" +
            "B077Z5L1HX\n" +
            "B077Z6836M\n" +
            "B077ZBSZHZ\n" +
            "B077ZMMQJY\n" +
            "B07814BV4K\n" +
            "B0781V5VNT\n" +
            "B0783MRN3N\n" +
            "B0783P74XK\n" +
            "B0784S9VXK\n" +
            "B0785H27M1\n" +
            "B0786CZ527\n" +
            "B0787Z74H5\n" +
            "B0789QNQ8V\n" +
            "B0789T2VXJ\n" +
            "B078BMRLLL\n" +
            "B078C5CWW2\n" +
            "B078HT191D\n" +
            "B078NP2NBT\n" +
            "B078NVR24Q\n" +
            "B078NWGMGX\n" +
            "B078NWVJTW\n" +
            "B078P74FT2\n" +
            "B078PBD54X\n" +
            "B078PNH1R1\n" +
            "B078RKD3MQ\n" +
            "B078S7Z5ZR\n" +
            "B078TFQDV4\n" +
            "B078W4W1QN\n" +
            "B078WR7QSH\n" +
            "B078WYLLDT\n" +
            "B078XYHT32\n" +
            "B078YPJTR9\n" +
            "B078YWQ8C8\n" +
            "B0792KTDCF\n" +
            "B0792NSR3J\n" +
            "B0792ZGY8N\n" +
            "B079339KDL\n" +
            "B07933R262\n" +
            "B0793RC2WF\n" +
            "B0794PL4GZ\n" +
            "B07952Z38W\n" +
            "B0796S6J63\n" +
            "B0796THK7Q\n" +
            "B0797RWRF7\n" +
            "B07989BF2C\n" +
            "B0798LF8Q7\n" +
            "B079BSSZT7\n" +
            "B079BVXP63\n" +
            "B079DJC2K6\n" +
            "B079GZ2QQQ\n" +
            "B079GZBV4Z\n" +
            "B079H2V7QL\n" +
            "B079HHC7KH\n" +
            "B079HSRB49\n" +
            "B079JZ3FY9\n" +
            "B079KT6XQG\n" +
            "B079L4W8MX\n" +
            "B079P76YD6\n" +
            "B079Q66698\n" +
            "B079W7GD67\n" +
            "B079XWPP1X\n" +
            "B079YSDMCQ\n" +
            "B079YX9MLR\n" +
            "B07B1138TD\n" +
            "B07B113DKB\n" +
            "B07B11ZKW1\n" +
            "B07B2Z773R\n" +
            "B07B38LCG9\n" +
            "B07B3JFJTT\n" +
            "B07B3V16Q2\n" +
            "B07B3YJDCH\n" +
            "B07B42CTZX\n" +
            "B07B4GR35P\n" +
            "B07B4W51T6\n" +
            "B07BBJP7MN\n" +
            "B07BBP153F\n" +
            "B07BCNPRYB\n" +
            "B07BDHH9VT\n" +
            "B07BDK8SS1\n" +
            "B07BDMJM3H\n" +
            "B07BGSYQ48\n" +
            "B07BH21DH9\n" +
            "B07BHKBDKZ\n" +
            "B07BHSJCDL\n" +
            "B07BKPPPN4\n" +
            "B07BKVD5K8\n" +
            "B07BKZ1XSH\n" +
            "B07BKZB121\n" +
            "B07BLRPYT6\n" +
            "B07BMWYWWQ\n" +
            "B07BP2N3S2\n" +
            "B07BQT3PMS\n" +
            "B07BR4SWR1\n" +
            "B07BR7N9BP\n" +
            "B07BRD91PG\n" +
            "B07BSFXK7H\n" +
            "B07BSM37SG\n" +
            "B07BST3Y7K\n" +
            "B07BT79Q38\n" +
            "B07BT96SW3\n" +
            "B07BTBV24R\n" +
            "B07BTF4PR8\n" +
            "B07BTFG3M8\n" +
            "B07BTJYNK6\n" +
            "B07BTL85K5\n" +
            "B07BVBW7K1\n" +
            "B07BWPQX2Z\n" +
            "B07BZ1HDXG\n" +
            "B07BZ389W5\n" +
            "B07BZ6NPNV\n" +
            "B07BZP5P3H\n" +
            "B07C1YJ4WS\n" +
            "B07C31DS4J\n" +
            "B07C5CJPQF\n" +
            "B07C5SXPWV\n" +
            "B07C6F7TJ8\n" +
            "B07C6HC36J\n" +
            "B07C73TZF7\n" +
            "B07C7L31DL\n" +
            "B07C7NLB1H\n" +
            "B07C7NW4BS\n" +
            "B07C7VP14K\n" +
            "B07C7YYKVS\n" +
            "B07C881GRJ\n" +
            "B07C8RDQG2\n" +
            "B07CC4K1Q1\n" +
            "B07CCKTHTW\n" +
            "B07CG4SPVS\n" +
            "B07CG9MFVJ\n" +
            "B07CGQ28J7\n" +
            "B07CHCMQNZ\n" +
            "B07CHMF53Q\n" +
            "B07CKQMG46\n" +
            "B07CL6TXNG\n" +
            "B07CLHNR6C\n" +
            "B07CLT3TSS\n" +
            "B07CMQH37M\n" +
            "B07CNFJ4SN\n" +
            "B07CP6W679\n" +
            "B07CPZ5368\n" +
            "B07CQBYHY5\n" +
            "B07CQW3KM1\n" +
            "B07CQW4H5Z\n" +
            "B07CSM59BF\n" +
            "B07CTC8467\n" +
            "B07CTKH2LM\n" +
            "B07CTL4J17\n" +
            "B07CTMBP2V\n" +
            "B07CTWSG93\n" +
            "B07CVC9FNZ\n" +
            "B07CVZ4DGQ\n" +
            "B07CWV92HF\n" +
            "B07CXP6JV4\n" +
            "B07CYVSWZG\n" +
            "B07CYX4SDY\n" +
            "B07CZ2W7QT\n" +
            "B07CZ6MSB4\n" +
            "B07CZ6XTDM\n" +
            "B07CZN8DTS\n" +
            "B07CZNNWZ9\n" +
            "B07CZQ7TL9\n" +
            "B07CZQB1QL\n" +
            "B07D13Y4XS\n" +
            "B07D14J7PY\n" +
            "B07D1834JW\n" +
            "B07D3M3DBZ\n" +
            "B07D3S1FLR\n" +
            "B07D3TBLV5\n" +
            "B07D4F4J5M\n" +
            "B07D4PQYRF\n" +
            "B07D4PST3X\n" +
            "B07D5CR2NN\n" +
            "B07D6RHZDC\n" +
            "B07D77JZKY\n" +
            "B07D7K3GV9\n" +
            "B07D7LCLPS\n" +
            "B07D7S89B7\n" +
            "B07D7ZC6J4\n" +
            "B07D8SWDFW\n" +
            "B07D8TYYT2\n" +
            "B07D97F3QB\n" +
            "B07D9H886H\n" +
            "B07D9M9M5S\n" +
            "B07DBCTP7L\n" +
            "B07DBDCFS1\n" +
            "B07DC3GYRR\n" +
            "B07DC5Y458\n" +
            "B07DCKN2ZW\n" +
            "B07DG3MBRB\n" +
            "B07DG3Y4P5\n" +
            "B07DH8B721\n" +
            "B07DHX6W7K\n" +
            "B07DJ74KJT\n" +
            "B07DJBJ3PS\n" +
            "B07DK3DR8S\n" +
            "B07DNNR11L\n" +
            "B07DNY841X\n" +
            "B07DPLG55Q\n" +
            "B07DQYZDLR\n" +
            "B07DRGJPFC\n" +
            "B07DRHMP4V\n" +
            "B07DWR1PKL\n" +
            "B07DWTLGPG\n" +
            "B07DXGZ8GZ\n" +
            "B07DZZYXTB\n" +
            "B07F1CVV33\n" +
            "B07F1QXFWV\n" +
            "B07F3BTT45\n" +
            "B07F3YW9QH\n" +
            "B07F42F1PQ\n" +
            "B07F45X346\n" +
            "B07F67NWGG\n" +
            "B07F74FV4L\n" +
            "B07F7RWRYF\n" +
            "B07FCHCXJ8\n" +
            "B07FCPMJN8\n" +
            "B07FD7GH5L\n" +
            "B07FDQYVV2\n" +
            "B07FDWNL7G\n" +
            "B07FHZC5RK\n" +
            "B07FL9TV61\n" +
            "B07FLK43GP\n" +
            "B07FLTQ5BD\n" +
            "B07FLX7H15\n" +
            "B07FMGL5V6\n" +
            "B07FNMQXPT\n" +
            "B07FNSNPK3\n" +
            "B07FPP7812\n" +
            "B07FQBZ9G3\n" +
            "B07FR53844\n" +
            "B07FR5SRVY\n" +
            "B07FRF3LNP\n" +
            "B07FS2R3DD\n" +
            "B07FSBFZQN\n" +
            "B07FSBH6XY\n" +
            "B07FSK3Z6G\n" +
            "B07FSK9B6Q\n" +
            "B07FSMJNTW\n" +
            "B07FT35Q1H\n" +
            "B07FTHHL8C\n" +
            "B07FVJ2YDS\n" +
            "B07FXLSC34\n" +
            "B07FXSD9G3\n" +
            "B07FYH8ZQV\n" +
            "B07FZ9PSTT\n" +
            "B07FZQTLNQ\n" +
            "B07FZSGSY8\n" +
            "B07G2JJB5R\n" +
            "B07G335TBV\n" +
            "B07G3BZ1YF\n" +
            "B07G3CS92Y\n" +
            "B07G3CXB9W\n" +
            "B07G3DNYK5\n" +
            "B07G457865\n" +
            "B07G5G2SF7\n" +
            "B07G9GNZXC\n" +
            "B07GCTY96S\n" +
            "B07GCYYNBN\n" +
            "B07GD6MD5K\n" +
            "B07GFJVH4T\n" +
            "B07GJP1GYG\n" +
            "B07GL16NQ7\n" +
            "B07GL3PGKD\n" +
            "B07GLFNZJW\n" +
            "B07GP22BNV\n" +
            "B07GSC56ZJ\n" +
            "B07GSZ2ZQW\n" +
            "B07GTJ3P9C\n" +
            "B07GVDQNFD\n" +
            "B07GVHWGQQ\n" +
            "B07GVJDC5L\n" +
            "B07GVKYJMW\n" +
            "B07GVL7BPP\n" +
            "B07GWQM8Q5\n" +
            "B07GWS6CNZ\n" +
            "B07GXNNCWX\n" +
            "B07H2WXGW5\n" +
            "B07H6BRT2N\n" +
            "B07H81BJRN\n" +
            "B07H8CFQC6\n" +
            "B07H8VHGBV\n" +
            "B07H9ZC7WD\n" +
            "B07HCBZTB7\n" +
            "B07HCYMVMK\n" +
            "B07HG4QWVJ\n" +
            "B07HG63W65\n" +
            "B07HKJTZFD\n" +
            "B07HKLY89Q\n" +
            "B07HL46VHN\n" +
            "B07HL5H4NC\n" +
            "B07HNZ2KMR\n" +
            "B07HP38FVW\n" +
            "B07HP4GH83\n" +
            "B07HQ92C39\n" +
            "B07HQQR976\n" +
            "B07HTYZSDT\n" +
            "B07HV7C9QZ\n" +
            "B07HWN32XB\n" +
            "B07HYQ32Q7\n" +
            "B07J36JJ8T\n" +
            "B07J6N8163\n" +
            "B07JCBLW32\n" +
            "B07JWLMHXT";

    static String guideNumber ="1001\n"+
            "1002\n"+
            "1003\n"+
            "1004\n"+
            "1005\n"+
            "1006\n"+
            "1007\n"+
            "1008\n"+
            "1009\n"+
            "1010\n"+
            "1011\n"+
            "1012\n"+
            "2001\n"+
            "2002\n"+
            "2003\n"+
            "2004\n"+
            "2005\n"+
            "2006\n"+
            "2007\n"+
            "2008\n"+
            "2009\n"+
            "2010\n"+
            "2011\n"+
            "2012\n"+
            "2013\n"+
            "2014\n"+
            "2015\n"+
            "2016\n"+
            "2017\n"+
            "2018\n"+
            "2019\n"+
            "2020\n"+
            "2021\n"+
            "3001\n"+
            "3002\n"+
            "3003\n"+
            "3004\n"+
            "3005\n"+
            "3006\n"+
            "3007\n"+
            "3008\n"+
            "3009\n"+
            "3010\n"+
            "3011\n"+
            "4001\n"+
            "4002\n"+
            "4003\n"+
            "4004\n"+
            "4005\n"+
            "4006\n"+
            "4007\n"+
            "5001\n"+
            "5002\n"+
            "5003\n"+
            "5004\n"+
            "5005\n"+
            "5006\n"+
            "5007\n"+
            "5008\n"+
            "5009\n"+
            "5010\n"+
            "5011\n"+
            "5012\n"+
            "6001\n"+
            "6002\n"+
            "6003\n"+
            "6004\n"+
            "6005\n"+
            "6006\n"+
            "6007\n"+
            "7001\n"+
            "7002\n"+
            "7003\n"+
            "7004\n"+
            "7005\n"+
            "7006\n"+
            "7007\n"+
            "8001\n"+
            "8002\n"+
            "8003\n"+
            "8004\n"+
            "8005\n"+
            "8006\n"+
            "8007\n"+
            "8008\n"+
            "8009\n"+
            "8010\n"+
            "8011\n"+
            "8012\n"+
            "8013\n"+
            "8014\n"+
            "8015\n"+
            "9001\n"+
            "9002\n"+
            "9003\n"+
            "9004\n"+
            "9005\n"+
            "9006\n"+
            "9007\n"+
            "9008\n"+
            "9009\n"+
            "10001\n"+
            "10002\n"+
            "10003\n"+
            "10004\n"+
            "10005\n"+
            "10006\n"+
            "10007\n"+
            "11001\n"+
            "11002\n"+
            "11003\n"+
            "11004\n"+
            "11005\n"+
            "11006\n"+
            "12001\n"+
            "12002\n"+
            "12003\n"+
            "12004\n"+
            "12005\n"+
            "12006\n"+
            "12007\n"+
            "12008\n"+
            "12009\n"+
            "13001\n"+
            "13002\n"+
            "13003\n"+
            "13004\n"+
            "13005\n"+
            "13006\n"+
            "13007\n"+
            "13008\n"+
            "14001\n"+
            "14002\n"+
            "14003\n"+
            "14004\n"+
            "14005\n"+
            "14006\n"+
            "14007\n"+
            "14008\n"+
            "15001\n"+
            "15002\n"+
            "15003\n"+
            "15004\n"+
            "15005\n"+
            "16001\n"+
            "16002\n"+
            "16003\n"+
            "16004\n"+
            "16005\n"+
            "16006\n"+
            "16007\n"+
            "16008\n"+
            "16009\n"+
            "16010\n"+
            "17001\n"+
            "17002\n"+
            "17003\n"+
            "17004\n"+
            "17005\n"+
            "17006\n"+
            "17007\n"+
            "17008\n"+
            "18001\n"+
            "18002\n"+
            "18003\n"+
            "18004\n"+
            "18005\n"+
            "18006\n"+
            "18007\n"+
            "19001\n"+
            "19002\n"+
            "19003\n"+
            "19004\n"+
            "19005\n"+
            "19006\n"+
            "20001\n"+
            "20002\n"+
            "20003\n"+
            "20004\n"+
            "20005\n"+
            "20006\n"+
            "21001\n"+
            "21002\n"+
            "21003\n"+
            "21004\n"+
            "21005\n"+
            "21006\n"+
            "21007\n"+
            "21008\n"+
            "22001\n"+
            "22002\n"+
            "22003\n"+
            "22004\n"+
            "22005\n"+
            "22006\n"+
            "22007\n"+
            "22008\n"+
            "22009\n"+
            "23001\n"+
            "23002\n"+
            "23003\n"+
            "23004\n"+
            "23005\n"+
            "24001\n"+
            "24002\n"+
            "24003\n"+
            "24004\n"+
            "24005\n"+
            "24006\n"+
            "24007\n"+
            "24008\n"+
            "25001\n"+
            "25002\n"+
            "25003\n"+
            "25004\n"+
            "25005\n"+
            "25006\n"+
            "25007\n"+
            "25008\n"+
            "25009\n"+
            "26001\n"+
            "26002\n"+
            "26003\n"+
            "26004\n"+
            "26005\n"+
            "26006\n"+
            "26007\n"+
            "26008\n"+
            "26009\n"+
            "27001\n"+
            "27002\n"+
            "27003\n"+
            "27004\n"+
            "27005\n"+
            "27006\n"+
            "27007\n"+
            "27008\n"+
            "27009\n"+
            "28001\n"+
            "28002\n"+
            "28003\n"+
            "28004\n"+
            "28005\n"+
            "28006\n"+
            "29001\n"+
            "29002\n"+
            "29003\n"+
            "29004\n"+
            "29005\n"+
            "29006\n"+
            "29007\n"+
            "30001\n"+
            "30002\n"+
            "30003\n"+
            "30004\n"+
            "30005\n"+
            "30006\n"+
            "30007\n"+
            "30008\n"+
            "30009\n"+
            "30010\n"+
            "30011\n"+
            "30012\n"+
            "30013\n"+
            "31001\n"+
            "31002\n"+
            "31003\n"+
            "31004\n"+
            "31005\n"+
            "32001\n"+
            "32002\n"+
            "32003\n"+
            "32004\n"+
            "32005\n"+
            "33001\n"+
            "33002\n"+
            "33003\n"+
            "33004\n"+
            "33005\n"+
            "34001\n"+
            "34002\n"+
            "34003\n"+
            "34004\n"+
            "35001\n"+
            "35002\n"+
            "35003\n"+
            "35004\n"+
            "35005\n"+
            "36001\n"+
            "36002\n"+
            "36003\n"+
            "36004\n"+
            "36005\n"+
            "37001\n"+
            "37002\n"+
            "37003\n"+
            "37004\n"+
            "37005\n"+
            "38001\n"+
            "38002\n"+
            "38003\n"+
            "38004\n"+
            "39001\n"+
            "39002\n"+
            "39003\n"+
            "39004\n"+
            "39005\n"+
            "40001\n"+
            "40002\n"+
            "40003\n"+
            "40004\n"+
            "40005\n"+
            "41001\n"+
            "41002\n"+
            "41003\n"+
            "41004\n"+
            "42001\n"+
            "42002\n"+
            "42003\n"+
            "43001\n"+
            "43002\n"+
            "43003\n"+
            "43004\n"+
            "43005\n"+
            "44001\n"+
            "44002\n"+
            "44003\n"+
            "44004\n"+
            "44005\n"+
            "45001\n"+
            "45002\n"+
            "45003\n"+
            "46001\n"+
            "46002\n"+
            "46003\n"+
            "46004\n"+
            "47001\n"+
            "47002\n"+
            "47003\n"+
            "47004\n"+
            "48001\n"+
            "48002\n"+
            "48003\n"+
            "48004\n"+
            "48005\n"+
            "49001\n"+
            "49002\n"+
            "49003\n"+
            "49004\n"+
            "49005\n"+
            "50001\n"+
            "50002\n"+
            "50003\n"+
            "51001\n"+
            "51002\n"+
            "51003\n"+
            "51004\n"+
            "51005\n"+
            "52001\n"+
            "52002\n"+
            "52003\n"+
            "52004\n"+
            "52005\n"+
            "53001\n"+
            "53002\n"+
            "53003\n"+
            "53004\n"+
            "53005\n"+
            "54001\n"+
            "54002\n"+
            "54003\n"+
            "55001\n"+
            "55002\n"+
            "55003\n"+
            "55004\n"+
            "55005\n"+
            "56001\n"+
            "56002\n"+
            "56003\n"+
            "56004\n"+
            "56005\n"+
            "57001\n"+
            "57002\n"+
            "57003\n"+
            "57004\n"+
            "57005\n"+
            "58001\n"+
            "58002\n"+
            "58003\n"+
            "58004\n"+
            "58005\n"+
            "59001\n"+
            "59002\n"+
            "59003\n"+
            "59004\n"+
            "59005\n"+
            "60001\n"+
            "60002\n"+
            "60003\n"+
            "61001\n"+
            "61002\n"+
            "61003\n"+
            "62001\n"+
            "62002\n"+
            "62003\n"+
            "62004\n"+
            "62005\n"+
            "63001\n"+
            "63002\n"+
            "63003\n"+
            "63004\n"+
            "64001\n"+
            "64002\n"+
            "64003\n"+
            "65001\n"+
            "65002\n"+
            "65003\n"+
            "65004\n"+
            "66001\n"+
            "66002\n"+
            "66003\n"+
            "67001\n"+
            "67002\n"+
            "67003\n"+
            "68001\n"+
            "68002\n"+
            "68003\n"+
            "68004\n"+
            "69001\n"+
            "69002\n"+
            "69003\n"+
            "69004\n"+
            "69005\n"+
            "70001\n"+
            "70002\n"+
            "70003\n"+
            "70004\n"+
            "70005\n"+
            "71001\n"+
            "71002\n"+
            "71003\n"+
            "72001\n"+
            "72002\n"+
            "72003\n"+
            "72004\n"+
            "73001\n"+
            "73002\n"+
            "73003\n"+
            "73004\n"+
            "73005\n"+
            "74001\n"+
            "74002\n"+
            "74003\n"+
            "74004\n"+
            "74005\n"+
            "75001\n"+
            "75002\n"+
            "75003\n"+
            "75004\n"+
            "75005\n"+
            "76001\n"+
            "76002\n"+
            "76003\n"+
            "76004\n"+
            "76005\n"+
            "77001\n"+
            "77002\n"+
            "77003\n"+
            "77004\n"+
            "78001\n"+
            "78002\n"+
            "78003\n"+
            "78004\n"+
            "78005\n"+
            "79001\n"+
            "79002\n"+
            "79003\n"+
            "79004\n"+
            "80001\n"+
            "80002\n"+
            "80003\n"+
            "80004\n"+
            "80005\n"+
            "81001\n"+
            "81002\n"+
            "81003\n"+
            "81004\n"+
            "81005\n"+
            "82001\n"+
            "82002\n"+
            "82003\n"+
            "82004\n"+
            "82005\n"+
            "83001\n"+
            "83002\n"+
            "83003\n"+
            "84001\n"+
            "84002\n"+
            "84003\n"+
            "85001\n"+
            "85002\n"+
            "85003\n"+
            "85004\n"+
            "86001\n"+
            "86002\n"+
            "86003\n"+
            "87001\n"+
            "87002\n"+
            "87003\n"+
            "87004\n"+
            "88001\n"+
            "88002\n"+
            "88003\n"+
            "89001\n"+
            "89002\n"+
            "89003\n"+
            "89004\n"+
            "90001\n"+
            "90002\n"+
            "90003\n"+
            "90004\n"+
            "90005";

    static String domains="chaussuresde-securite.fr\n" +
            "chaussuresde-securite.fr\n" +
            "chaussuresde-securite.fr\n" +
            "chaussuresde-securite.fr\n" +
            "chaussuresde-securite.fr\n" +
            "chaussuresde-securite.fr\n" +
            "chaussuresde-securite.fr\n" +
            "chaussuresde-securite.fr\n" +
            "chaussuresde-securite.fr\n" +
            "chaussuresde-securite.fr\n" +
            "chaussuresde-securite.fr\n" +
            "chaussuresde-securite.fr\n" +
            "meilleure-couette.fr\n" +
            "meilleure-couette.fr\n" +
            "meilleure-couette.fr\n" +
            "meilleure-couette.fr\n" +
            "meilleure-couette.fr\n" +
            "meilleure-couette.fr\n" +
            "meilleure-couette.fr\n" +
            "meilleure-couette.fr\n" +
            "meilleure-couette.fr\n" +
            "meilleure-couette.fr\n" +
            "meilleure-couette.fr\n" +
            "meilleure-couette.fr\n" +
            "meilleure-couette.fr\n" +
            "meilleure-couette.fr\n" +
            "meilleure-couette.fr\n" +
            "meilleure-couette.fr\n" +
            "meilleure-couette.fr\n" +
            "meilleure-couette.fr\n" +
            "meilleure-couette.fr\n" +
            "meilleure-couette.fr\n" +
            "meilleure-couette.fr\n" +
            "meilleur-collier-dressage.fr\n" +
            "meilleur-collier-dressage.fr\n" +
            "meilleur-collier-dressage.fr\n" +
            "meilleur-collier-dressage.fr\n" +
            "meilleur-collier-dressage.fr\n" +
            "meilleur-collier-dressage.fr\n" +
            "meilleur-collier-dressage.fr\n" +
            "meilleur-collier-dressage.fr\n" +
            "meilleur-collier-dressage.fr\n" +
            "meilleur-collier-dressage.fr\n" +
            "meilleur-collier-dressage.fr\n" +
            "meilleur-rameur.fr\n" +
            "meilleur-rameur.fr\n" +
            "meilleur-rameur.fr\n" +
            "meilleur-rameur.fr\n" +
            "meilleur-rameur.fr\n" +
            "meilleur-rameur.fr\n" +
            "meilleur-rameur.fr\n" +
            "meilleur-coupebordure.fr\n" +
            "meilleur-coupebordure.fr\n" +
            "meilleur-coupebordure.fr\n" +
            "meilleur-coupebordure.fr\n" +
            "meilleur-coupebordure.fr\n" +
            "meilleur-coupebordure.fr\n" +
            "meilleur-coupebordure.fr\n" +
            "meilleur-coupebordure.fr\n" +
            "meilleur-coupebordure.fr\n" +
            "meilleur-coupebordure.fr\n" +
            "meilleur-coupebordure.fr\n" +
            "meilleur-coupebordure.fr\n" +
            "meilleur-tricycleevolutif.fr\n" +
            "meilleur-tricycleevolutif.fr\n" +
            "meilleur-tricycleevolutif.fr\n" +
            "meilleur-tricycleevolutif.fr\n" +
            "meilleur-tricycleevolutif.fr\n" +
            "meilleur-tricycleevolutif.fr\n" +
            "meilleur-tricycleevolutif.fr\n" +
            "meilleur-veloelliptique.fr\n" +
            "meilleur-veloelliptique.fr\n" +
            "meilleur-veloelliptique.fr\n" +
            "meilleur-veloelliptique.fr\n" +
            "meilleur-veloelliptique.fr\n" +
            "meilleur-veloelliptique.fr\n" +
            "meilleur-veloelliptique.fr\n" +
            "meilleur-pompederelevage.fr\n" +
            "meilleur-pompederelevage.fr\n" +
            "meilleur-pompederelevage.fr\n" +
            "meilleur-pompederelevage.fr\n" +
            "meilleur-pompederelevage.fr\n" +
            "meilleur-pompederelevage.fr\n" +
            "meilleur-pompederelevage.fr\n" +
            "meilleur-pompederelevage.fr\n" +
            "meilleur-pompederelevage.fr\n" +
            "meilleur-pompederelevage.fr\n" +
            "meilleur-pompederelevage.fr\n" +
            "meilleur-pompederelevage.fr\n" +
            "meilleur-pompederelevage.fr\n" +
            "meilleur-pompederelevage.fr\n" +
            "meilleur-pompederelevage.fr\n" +
            "meilleure-machineaglace.fr\n" +
            "meilleure-machineaglace.fr\n" +
            "meilleure-machineaglace.fr\n" +
            "meilleure-machineaglace.fr\n" +
            "meilleure-machineaglace.fr\n" +
            "meilleure-machineaglace.fr\n" +
            "meilleure-machineaglace.fr\n" +
            "meilleure-machineaglace.fr\n" +
            "meilleure-machineaglace.fr\n" +
            "top-ceinture-abdo.fr\n" +
            "top-ceinture-abdo.fr\n" +
            "top-ceinture-abdo.fr\n" +
            "top-ceinture-abdo.fr\n" +
            "top-ceinture-abdo.fr\n" +
            "top-ceinture-abdo.fr\n" +
            "top-ceinture-abdo.fr\n" +
            "top-cardiofrequencemetre.fr\n" +
            "top-cardiofrequencemetre.fr\n" +
            "top-cardiofrequencemetre.fr\n" +
            "top-cardiofrequencemetre.fr\n" +
            "top-cardiofrequencemetre.fr\n" +
            "top-cardiofrequencemetre.fr\n" +
            "meilleur-rasage.fr\n" +
            "meilleur-rasage.fr\n" +
            "meilleur-rasage.fr\n" +
            "meilleur-rasage.fr\n" +
            "meilleur-rasage.fr\n" +
            "meilleur-rasage.fr\n" +
            "meilleur-rasage.fr\n" +
            "meilleur-rasage.fr\n" +
            "meilleur-rasage.fr\n" +
            "meilleure-sciesauteuse.fr\n" +
            "meilleure-sciesauteuse.fr\n" +
            "meilleure-sciesauteuse.fr\n" +
            "meilleure-sciesauteuse.fr\n" +
            "meilleure-sciesauteuse.fr\n" +
            "meilleure-sciesauteuse.fr\n" +
            "meilleure-sciesauteuse.fr\n" +
            "meilleure-sciesauteuse.fr\n" +
            "meilleur-brisevue.fr\n" +
            "meilleur-brisevue.fr\n" +
            "meilleur-brisevue.fr\n" +
            "meilleur-brisevue.fr\n" +
            "meilleur-brisevue.fr\n" +
            "meilleur-brisevue.fr\n" +
            "meilleur-brisevue.fr\n" +
            "meilleur-brisevue.fr\n" +
            "meilleur-poulailler.fr\n" +
            "meilleur-poulailler.fr\n" +
            "meilleur-poulailler.fr\n" +
            "meilleur-poulailler.fr\n" +
            "meilleur-poulailler.fr\n" +
            "top-compresseur.fr\n" +
            "top-compresseur.fr\n" +
            "top-compresseur.fr\n" +
            "top-compresseur.fr\n" +
            "top-compresseur.fr\n" +
            "top-compresseur.fr\n" +
            "top-compresseur.fr\n" +
            "top-compresseur.fr\n" +
            "top-compresseur.fr\n" +
            "top-compresseur.fr\n" +
            "meilleure-sciesurtable.fr\n" +
            "meilleure-sciesurtable.fr\n" +
            "meilleure-sciesurtable.fr\n" +
            "meilleure-sciesurtable.fr\n" +
            "meilleure-sciesurtable.fr\n" +
            "meilleure-sciesurtable.fr\n" +
            "meilleure-sciesurtable.fr\n" +
            "meilleure-sciesurtable.fr\n" +
            "poele-enpierre.fr\n" +
            "poele-enpierre.fr\n" +
            "poele-enpierre.fr\n" +
            "poele-enpierre.fr\n" +
            "poele-enpierre.fr\n" +
            "poele-enpierre.fr\n" +
            "poele-enpierre.fr\n" +
            "machinea-pain.fr\n" +
            "machinea-pain.fr\n" +
            "machinea-pain.fr\n" +
            "machinea-pain.fr\n" +
            "machinea-pain.fr\n" +
            "machinea-pain.fr\n" +
            "meilleur-blenderchauffant.fr\n" +
            "meilleur-blenderchauffant.fr\n" +
            "meilleur-blenderchauffant.fr\n" +
            "meilleur-blenderchauffant.fr\n" +
            "meilleur-blenderchauffant.fr\n" +
            "meilleur-blenderchauffant.fr\n" +
            "meilleure-crepiere.fr\n" +
            "meilleure-crepiere.fr\n" +
            "meilleure-crepiere.fr\n" +
            "meilleure-crepiere.fr\n" +
            "meilleure-crepiere.fr\n" +
            "meilleure-crepiere.fr\n" +
            "meilleure-crepiere.fr\n" +
            "meilleure-crepiere.fr\n" +
            "scieradiale.fr\n" +
            "scieradiale.fr\n" +
            "scieradiale.fr\n" +
            "scieradiale.fr\n" +
            "scieradiale.fr\n" +
            "scieradiale.fr\n" +
            "scieradiale.fr\n" +
            "scieradiale.fr\n" +
            "scieradiale.fr\n" +
            "top-theireelectrique.fr\n" +
            "top-theireelectrique.fr\n" +
            "top-theireelectrique.fr\n" +
            "top-theireelectrique.fr\n" +
            "top-theireelectrique.fr\n" +
            "couper-au-couteau.fr\n" +
            "couper-au-couteau.fr\n" +
            "couper-au-couteau.fr\n" +
            "couper-au-couteau.fr\n" +
            "couper-au-couteau.fr\n" +
            "couper-au-couteau.fr\n" +
            "couper-au-couteau.fr\n" +
            "couper-au-couteau.fr\n" +
            "trancheusejambon.fr\n" +
            "trancheusejambon.fr\n" +
            "trancheusejambon.fr\n" +
            "trancheusejambon.fr\n" +
            "trancheusejambon.fr\n" +
            "trancheusejambon.fr\n" +
            "trancheusejambon.fr\n" +
            "trancheusejambon.fr\n" +
            "trancheusejambon.fr\n" +
            "top-servanteatelier.fr\n" +
            "top-servanteatelier.fr\n" +
            "top-servanteatelier.fr\n" +
            "top-servanteatelier.fr\n" +
            "top-servanteatelier.fr\n" +
            "top-servanteatelier.fr\n" +
            "top-servanteatelier.fr\n" +
            "top-servanteatelier.fr\n" +
            "top-servanteatelier.fr\n" +
            "meilleure-sciecirculaire.fr\n" +
            "meilleure-sciecirculaire.fr\n" +
            "meilleure-sciecirculaire.fr\n" +
            "meilleure-sciecirculaire.fr\n" +
            "meilleure-sciecirculaire.fr\n" +
            "meilleure-sciecirculaire.fr\n" +
            "meilleure-sciecirculaire.fr\n" +
            "meilleure-sciecirculaire.fr\n" +
            "meilleure-sciecirculaire.fr\n" +
            "meilleure-scieachantourner.fr\n" +
            "meilleure-scieachantourner.fr\n" +
            "meilleure-scieachantourner.fr\n" +
            "meilleure-scieachantourner.fr\n" +
            "meilleure-scieachantourner.fr\n" +
            "meilleure-scieachantourner.fr\n" +
            "meilleure-peluchegeante.fr\n" +
            "meilleure-peluchegeante.fr\n" +
            "meilleure-peluchegeante.fr\n" +
            "meilleure-peluchegeante.fr\n" +
            "meilleure-peluchegeante.fr\n" +
            "meilleure-peluchegeante.fr\n" +
            "meilleure-peluchegeante.fr\n" +
            "epilateurslumierepulsee.fr\n" +
            "epilateurslumierepulsee.fr\n" +
            "epilateurslumierepulsee.fr\n" +
            "epilateurslumierepulsee.fr\n" +
            "epilateurslumierepulsee.fr\n" +
            "epilateurslumierepulsee.fr\n" +
            "epilateurslumierepulsee.fr\n" +
            "epilateurslumierepulsee.fr\n" +
            "epilateurslumierepulsee.fr\n" +
            "epilateurslumierepulsee.fr\n" +
            "epilateurslumierepulsee.fr\n" +
            "epilateurslumierepulsee.fr\n" +
            "epilateurslumierepulsee.fr\n" +
            "top-chatiere-electronique.fr\n" +
            "top-chatiere-electronique.fr\n" +
            "top-chatiere-electronique.fr\n" +
            "top-chatiere-electronique.fr\n" +
            "top-chatiere-electronique.fr\n" +
            "super-aspirateur-silencieux.fr\n" +
            "super-aspirateur-silencieux.fr\n" +
            "super-aspirateur-silencieux.fr\n" +
            "super-aspirateur-silencieux.fr\n" +
            "super-aspirateur-silencieux.fr\n" +
            "meilleure-mijoteuse.fr\n" +
            "meilleure-mijoteuse.fr\n" +
            "meilleure-mijoteuse.fr\n" +
            "meilleure-mijoteuse.fr\n" +
            "meilleure-mijoteuse.fr\n" +
            "meilleur-sechoir-linge.fr\n" +
            "meilleur-sechoir-linge.fr\n" +
            "meilleur-sechoir-linge.fr\n" +
            "meilleur-sechoir-linge.fr\n" +
            "ma-trancheuse-saucission.fr\n" +
            "ma-trancheuse-saucission.fr\n" +
            "ma-trancheuse-saucission.fr\n" +
            "ma-trancheuse-saucission.fr\n" +
            "ma-trancheuse-saucission.fr\n" +
            "top-tricycle-adulte.fr\n" +
            "top-tricycle-adulte.fr\n" +
            "top-tricycle-adulte.fr\n" +
            "top-tricycle-adulte.fr\n" +
            "top-tricycle-adulte.fr\n" +
            "top-essoreuse-salade.fr\n" +
            "top-essoreuse-salade.fr\n" +
            "top-essoreuse-salade.fr\n" +
            "top-essoreuse-salade.fr\n" +
            "top-essoreuse-salade.fr\n" +
            "meilleur-secateur-electrique.fr\n" +
            "meilleur-secateur-electrique.fr\n" +
            "meilleur-secateur-electrique.fr\n" +
            "meilleur-secateur-electrique.fr\n" +
            "top-coupe-frite.fr\n" +
            "top-coupe-frite.fr\n" +
            "top-coupe-frite.fr\n" +
            "top-coupe-frite.fr\n" +
            "top-coupe-frite.fr\n" +
            "top-ventilateur-pied.fr\n" +
            "top-ventilateur-pied.fr\n" +
            "top-ventilateur-pied.fr\n" +
            "top-ventilateur-pied.fr\n" +
            "top-ventilateur-pied.fr\n" +
            "portebebe-physiologique.fr\n" +
            "portebebe-physiologique.fr\n" +
            "portebebe-physiologique.fr\n" +
            "portebebe-physiologique.fr\n" +
            "top-coupe-branche.fr\n" +
            "top-coupe-branche.fr\n" +
            "top-coupe-branche.fr\n" +
            "meilleure-perceuse-colonne.fr\n" +
            "meilleure-perceuse-colonne.fr\n" +
            "meilleure-perceuse-colonne.fr\n" +
            "meilleure-perceuse-colonne.fr\n" +
            "meilleure-perceuse-colonne.fr\n" +
            "top-pese-bebe.fr\n" +
            "top-pese-bebe.fr\n" +
            "top-pese-bebe.fr\n" +
            "top-pese-bebe.fr\n" +
            "top-pese-bebe.fr\n" +
            "chauffeeau-instantane.fr\n" +
            "chauffeeau-instantane.fr\n" +
            "chauffeeau-instantane.fr\n" +
            "machine-glacons.fr\n" +
            "machine-glacons.fr\n" +
            "machine-glacons.fr\n" +
            "machine-glacons.fr\n" +
            "friteuse-sanshuile.fr\n" +
            "friteuse-sanshuile.fr\n" +
            "friteuse-sanshuile.fr\n" +
            "friteuse-sanshuile.fr\n" +
            "meilleur-hachoir-viande.fr\n" +
            "meilleur-hachoir-viande.fr\n" +
            "meilleur-hachoir-viande.fr\n" +
            "meilleur-hachoir-viande.fr\n" +
            "meilleur-hachoir-viande.fr\n" +
            "top-batteur-electrique.fr\n" +
            "top-batteur-electrique.fr\n" +
            "top-batteur-electrique.fr\n" +
            "top-batteur-electrique.fr\n" +
            "top-batteur-electrique.fr\n" +
            "meilleurpercolateur.fr\n" +
            "meilleurpercolateur.fr\n" +
            "meilleurpercolateur.fr\n" +
            "tirelait-electrique.fr\n" +
            "tirelait-electrique.fr\n" +
            "tirelait-electrique.fr\n" +
            "tirelait-electrique.fr\n" +
            "tirelait-electrique.fr\n" +
            "top-lit-pliant.fr\n" +
            "top-lit-pliant.fr\n" +
            "top-lit-pliant.fr\n" +
            "top-lit-pliant.fr\n" +
            "top-lit-pliant.fr\n" +
            "top-aspirateurvoiture.Fr\n" +
            "top-aspirateurvoiture.Fr\n" +
            "top-aspirateurvoiture.Fr\n" +
            "top-aspirateurvoiture.Fr\n" +
            "top-aspirateurvoiture.Fr\n" +
            "motobineuseelectrique.fr\n" +
            "motobineuseelectrique.fr\n" +
            "motobineuseelectrique.fr\n" +
            "meilleure-glaciereelectrique.Fr\n" +
            "meilleure-glaciereelectrique.Fr\n" +
            "meilleure-glaciereelectrique.Fr\n" +
            "meilleure-glaciereelectrique.Fr\n" +
            "meilleure-glaciereelectrique.Fr\n" +
            "balancellejardin.fr\n" +
            "balancellejardin.fr\n" +
            "balancellejardin.fr\n" +
            "balancellejardin.fr\n" +
            "balancellejardin.fr\n" +
            "meilleure-poussettetrio.fr\n" +
            "meilleure-poussettetrio.fr\n" +
            "meilleure-poussettetrio.fr\n" +
            "meilleure-poussettetrio.fr\n" +
            "meilleure-poussettetrio.fr\n" +
            "meilleur-babyphonevideo.fr\n" +
            "meilleur-babyphonevideo.fr\n" +
            "meilleur-babyphonevideo.fr\n" +
            "meilleur-babyphonevideo.fr\n" +
            "meilleur-babyphonevideo.fr\n" +
            "top-balancellebebe.fr\n" +
            "top-balancellebebe.fr\n" +
            "top-balancellebebe.fr\n" +
            "top-balancellebebe.fr\n" +
            "top-balancellebebe.fr\n" +
            "top-souffleur-thermique.fr\n" +
            "top-souffleur-thermique.fr\n" +
            "top-souffleur-thermique.fr\n" +
            "tests-ventilateur-silencieux.fr\n" +
            "tests-ventilateur-silencieux.fr\n" +
            "tests-ventilateur-silencieux.fr\n" +
            "tests-machine-a-pop-corn.fr\n" +
            "tests-machine-a-pop-corn.fr\n" +
            "tests-machine-a-pop-corn.fr\n" +
            "tests-machine-a-pop-corn.fr\n" +
            "tests-machine-a-pop-corn.fr\n" +
            "tests-tuyau-arrosage-extensible.fr\n" +
            "tests-tuyau-arrosage-extensible.fr\n" +
            "tests-tuyau-arrosage-extensible.fr\n" +
            "tests-tuyau-arrosage-extensible.fr\n" +
            "tests-theiere-electrique.fr\n" +
            "tests-theiere-electrique.fr\n" +
            "tests-theiere-electrique.fr\n" +
            "tests-inhalateur.fr\n" +
            "tests-inhalateur.fr\n" +
            "tests-inhalateur.fr\n" +
            "tests-inhalateur.fr\n" +
            "tests-tondeuse-manuelle.fr\n" +
            "tests-tondeuse-manuelle.fr\n" +
            "tests-tondeuse-manuelle.fr\n" +
            "tests-aspirateur-de-table.fr\n" +
            "tests-aspirateur-de-table.fr\n" +
            "tests-aspirateur-de-table.fr\n" +
            "tests-souffleur-thermique.fr\n" +
            "tests-souffleur-thermique.fr\n" +
            "tests-souffleur-thermique.fr\n" +
            "tests-souffleur-thermique.fr\n" +
            "tests-aspirateur-a-cendre.fr\n" +
            "tests-aspirateur-a-cendre.fr\n" +
            "tests-aspirateur-a-cendre.fr\n" +
            "tests-aspirateur-a-cendre.fr\n" +
            "tests-aspirateur-a-cendre.fr\n" +
            "tests-ventilateur-pas-cher.fr\n" +
            "tests-ventilateur-pas-cher.fr\n" +
            "tests-ventilateur-pas-cher.fr\n" +
            "tests-ventilateur-pas-cher.fr\n" +
            "tests-ventilateur-pas-cher.fr\n" +
            "tests-appareil-photo-enfant.fr\n" +
            "tests-appareil-photo-enfant.fr\n" +
            "tests-appareil-photo-enfant.fr\n" +
            "tests-pluviometre.fr\n" +
            "tests-pluviometre.fr\n" +
            "tests-pluviometre.fr\n" +
            "tests-pluviometre.fr\n" +
            "tests-relieuse.fr\n" +
            "tests-relieuse.fr\n" +
            "tests-relieuse.fr\n" +
            "tests-relieuse.fr\n" +
            "tests-relieuse.fr\n" +
            "tests-cocotte-en-fonte.fr\n" +
            "tests-cocotte-en-fonte.fr\n" +
            "tests-cocotte-en-fonte.fr\n" +
            "tests-cocotte-en-fonte.fr\n" +
            "tests-cocotte-en-fonte.fr\n" +
            "tests-scie-radiale.fr\n" +
            "tests-scie-radiale.fr\n" +
            "tests-scie-radiale.fr\n" +
            "tests-scie-radiale.fr\n" +
            "tests-scie-radiale.fr\n" +
            "tests-chaussure-bateau.fr\n" +
            "tests-chaussure-bateau.fr\n" +
            "tests-chaussure-bateau.fr\n" +
            "tests-chaussure-bateau.fr\n" +
            "tests-chaussure-bateau.fr\n" +
            "tests-sterilisateur-biberon.fr\n" +
            "tests-sterilisateur-biberon.fr\n" +
            "tests-sterilisateur-biberon.fr\n" +
            "tests-sterilisateur-biberon.fr\n" +
            "tests-vasques-a-poser.fr\n" +
            "tests-vasques-a-poser.fr\n" +
            "tests-vasques-a-poser.fr\n" +
            "tests-vasques-a-poser.fr\n" +
            "tests-vasques-a-poser.fr\n" +
            "tests-arbre-a-chat.fr\n" +
            "tests-arbre-a-chat.fr\n" +
            "tests-arbre-a-chat.fr\n" +
            "tests-arbre-a-chat.fr\n" +
            "tests-patin-a-roulette.fr\n" +
            "tests-patin-a-roulette.fr\n" +
            "tests-patin-a-roulette.fr\n" +
            "tests-patin-a-roulette.fr\n" +
            "tests-patin-a-roulette.fr\n" +
            "tests-brosse-lissante.fr\n" +
            "tests-brosse-lissante.fr\n" +
            "tests-brosse-lissante.fr\n" +
            "tests-brosse-lissante.fr\n" +
            "tests-brosse-lissante.fr\n" +
            "tests-poele-induction.fr\n" +
            "tests-poele-induction.fr\n" +
            "tests-poele-induction.fr\n" +
            "tests-poele-induction.fr\n" +
            "tests-poele-induction.fr\n" +
            "tests-ponceuse-a-bande.fr\n" +
            "tests-ponceuse-a-bande.fr\n" +
            "tests-ponceuse-a-bande.fr\n" +
            "tests-broyeur-de-branches.fr\n" +
            "tests-broyeur-de-branches.fr\n" +
            "tests-broyeur-de-branches.fr\n" +
            "tests-casque-moto-cross.fr\n" +
            "tests-casque-moto-cross.fr\n" +
            "tests-casque-moto-cross.fr\n" +
            "tests-casque-moto-cross.fr\n" +
            "tests-rehausseur-de-chaise.fr\n" +
            "tests-rehausseur-de-chaise.fr\n" +
            "tests-rehausseur-de-chaise.fr\n" +
            "tests-niche-pour-chien.fr\n" +
            "tests-niche-pour-chien.fr\n" +
            "tests-niche-pour-chien.fr\n" +
            "tests-niche-pour-chien.fr\n" +
            "tests-pompe-vide-cave.fr\n" +
            "tests-pompe-vide-cave.fr\n" +
            "tests-pompe-vide-cave.fr\n" +
            "tests-appareil-de-musculation.fr\n" +
            "tests-appareil-de-musculation.fr\n" +
            "tests-appareil-de-musculation.fr\n" +
            "tests-appareil-de-musculation.fr\n" +
            "tests-ponceuse.fr\n" +
            "tests-ponceuse.fr\n" +
            "tests-ponceuse.fr\n" +
            "tests-ponceuse.fr\n" +
            "tests-ponceuse.fr";
    static String guideName="chaussure de securite\n" +
            "chaussure de securite femme\n" +
            "chaussure de securite femme legere\n" +
            "chaussure de securite legere \n" +
            "basket de securite\n" +
            "chaussure de securite puma\n" +
            "chaussure de securite timberland\n" +
            "basket de securite femme\n" +
            "chaussure de securite montante\n" +
            "chaussure de sécurité confortable\n" +
            "chaussure de securite femme confortable\n" +
            "botte de sécurité\n" +
            "couette dodo\n" +
            "couette bébé\n" +
            "couette enfant\n" +
            "couette chaude\n" +
            "couette d été\n" +
            "housse de couette plume\n" +
            "housse de couette coton\n" +
            "couette tres chaude\n" +
            "couette anti acarien\n" +
            "couette en soie\n" +
            "couette legere\n" +
            "couette naturelle\n" +
            "couette thermorégulatrice\n" +
            "couette microfibre\n" +
            "couette plume \n" +
            "couette pas cher\n" +
            "couette duvet\n" +
            "housse de couette satin de coton\n" +
            "couette hiver\n" +
            "couette duvet doie\n" +
            "housse de couette percale\n" +
            "collier de dressage pour chien de chasse\n" +
            "collier anti fugue chien\n" +
            "collier de dressage\n" +
            "collier electrique pour chien\n" +
            "harnais pour petit chien\n" +
            "harnais ruffwear\n" +
            "collier anti aboiement citronnelle\n" +
            "harnet pour chien\n" +
            "harnais chiot\n" +
            "collier petsafe\n" +
            "collier dogtra\n" +
            "rameur concept 2\n" +
            "rameur d appartement\n" +
            "rameur pliable\n" +
            "rameur scandinave\n" +
            "rameur kettler\n" +
            "rameur pas cher\n" +
            "rameur à eau\n" +
            "coupe bordure\n" +
            "coupe bordure sans fil\n" +
            "coupe bordure thermique\n" +
            "coupe bordure electrique\n" +
            "coupe bordure batterie\n" +
            "coupe bordure bosch\n" +
            "coupe bordure black et decker\n" +
            "coupe bordure pas cher\n" +
            "coupe bordure electrique bosch\n" +
            "coupe bordure bosch sans fil\n" +
            "coupe bordure black et decker sans fil\n" +
            "coupe bordure batterie bosch\n" +
            "tricycle évolutif\n" +
            "tricycle evolutif smoby\n" +
            "tricycle evolutif bebe\n" +
            "tricycle evolutif pas cher\n" +
            "tricycle évolutif smart trike\n" +
            "tricycle evolutif fille\n" +
            "tricycle evolutif feber\n" +
            "velo elliptique pas cher\n" +
            "velo elliptique pliable\n" +
            "velo elliptique proform\n" +
            "velo elliptique nordictrack \n" +
            "velo elliptique care \n" +
            "velo elliptique professionnel\n" +
            "velo elliptique striale\n" +
            "pompe de relevage\n" +
            "pompe de relevage eaux usées\n" +
            "station de relevage\n" +
            "pompe de relevage eaux chargées\n" +
            "pompe de relevage assainissement\n" +
            "pompe de relevage machine a laver\n" +
            "pompe forage\n" +
            "pompe immergée\n" +
            "pompe de surface\n" +
            "motopompe\n" +
            "pompe puit\n" +
            "pompe surpresseur\n" +
            "pompe d arrosage\n" +
            "pompe gasoil\n" +
            "pompe vide cave\n" +
            "turbine a glace\n" +
            "machine a glace\n" +
            "machine à glace pilée\n" +
            "machine a glace professionnel\n" +
            "sorbetière\n" +
            "sorbetière cuisinart\n" +
            "sorbetière lagrange\n" +
            "sorbetière manuelle\n" +
            "sorbetière turbine à glace\n" +
            "ceinture abdominale\n" +
            "ceinture abdominale slendertone\n" +
            "ceinture abdominale electrostimulation\n" +
            "ceinture abdominale amincissante\n" +
            "ceinture abdominale beurer\n" +
            "ceinture abdominale femme\n" +
            "ceinture abdominale homme\n" +
            "cardiofrequencemetre\n" +
            "cardiofrequencemetre polar\n" +
            "cardiofrequencemetre garmin\n" +
            "cardiofrequencemetre bluetooth\n" +
            "cardiofrequencemetre velo\n" +
            "cardiofrequencemetre gps\n" +
            "tondeuse barbe\n" +
            "tondeuse corps homme\n" +
            "meilleur epilateur\n" +
            "brosse a barbe\n" +
            "cire cheveux\n" +
            "peigne barbe\n" +
            "tondeuse bikini\n" +
            "rasoir de sureté\n" +
            "huile pour barbe\n" +
            "scie sauteuse bosch\n" +
            "scie sauteuse makita\n" +
            "scie sauteuse feestool\n" +
            "scie sauteuse black et decker\n" +
            "scie sauteuse pendulaire\n" +
            "scie sauteuse dewalt\n" +
            "scie sauteuse hitachi\n" +
            "scie sauteuse skil\n" +
            "brise vue retractable\n" +
            "brise vue pvc\n" +
            "brise vue occultant\n" +
            "brise vue toile\n" +
            "brise vue aluminium\n" +
            "brise vue naturel\n" +
            "brise vue haie artificielle\n" +
            "brise vue jardin\n" +
            "poulailler\n" +
            "poulailler 6 poules\n" +
            "poulailler 2 poules\n" +
            "poulailler en bois\n" +
            "poulailler avec enclos\n" +
            "compresseur 100l\n" +
            "compresseur portatif\n" +
            "compresseur 200l\n" +
            "compresseur portable\n" +
            "compresseur 150l\n" +
            "compresseur black et decker\n" +
            "compresseur 12v\n" +
            "compresseur lacme\n" +
            "compresseur a vis\n" +
            "compresseur aerographe\n" +
            "scie sur table\n" +
            "scie sur table makita\n" +
            "scie sur table pro\n" +
            "scie sur table dewalt\n" +
            "scie sur table evolution\n" +
            "scie sur table ryobi\n" +
            "scie sur table festool\n" +
            "scie sur table bosch\n" +
            "poêle en pierre\n" +
            "poele en pierre de lave\n" +
            "poele en pierre schumann\n" +
            "poele en pierre tefal\n" +
            "poele en pierre pradel\n" +
            "poele a bois pierre ollaire\n" +
            "poêle en pierre manche amovible\n" +
            "machine à pain\n" +
            "machine à pain panasonic\n" +
            "machine à pain kenwood\n" +
            "machine à pain sans gluten\n" +
            "machine à pain riviera et bar\n" +
            "machine à pain pas cher\n" +
            "blender chauffant\n" +
            "blender chauffant moulinex\n" +
            "blender chauffant philips\n" +
            "blender chauffant russell hobbs\n" +
            "blender chauffant pas cher\n" +
            "blender chauffant domoclip\n" +
            "crepiere\n" +
            "crepiere electrique\n" +
            "crepiere lagrange\n" +
            "crepiere tefal\n" +
            "crepiere krampouz\n" +
            "crepiere professionnelle\n" +
            "crepiere party\n" +
            "crepiere pas cher\n" +
            "scie radiale\n" +
            "scie radiale metabo\n" +
            "scie radiale makita\n" +
            "scie radiale bosch\n" +
            "scie radiale dewalt\n" +
            "scie radiale à onglet\n" +
            "scie radiale einhell\n" +
            "scie radiale bosch pro\n" +
            "scie radiale evolution\n" +
            "theiere electrique\n" +
            "theiere electrique tefal\n" +
            "theiere electrique pas cher\n" +
            "théière electrique riviera bar\n" +
            "théière automatique\n" +
            "couteau de survie\n" +
            "couteau de chasse\n" +
            "couteau de poche\n" +
            "couteau de poche pliant\n" +
            "couteau de poche laguiole\n" +
            "couteau de poche japonais\n" +
            "couteau de chasse pliant\n" +
            "petit couteau de poche\n" +
            "trancheuse\n" +
            "trancheuse jambon\n" +
            "trancheuse saucisson\n" +
            "trancheuse manuelle\n" +
            "trancheuse charcuterie\n" +
            "trancheuse professionnelle\n" +
            "trancheuse à pain\n" +
            "trancheuse berkel\n" +
            "trancheuse magimix\n" +
            "servante atelier\n" +
            "servante atelier pas cher\n" +
            "servante atelier complete\n" +
            "servante mobile\n" +
            "servante sam\n" +
            "servante à rouleau\n" +
            "servante facom complete\n" +
            "servante kraftwerk\n" +
            "servante atelier facom\n" +
            "scie circulaire plongeante\n" +
            "scie circulaire makita\n" +
            "scie circulaire parkside\n" +
            "scie circulaire festool\n" +
            "scie circulaire bosch\n" +
            "scie circulaire metal\n" +
            "scie circulaire metabo\n" +
            "scie circulaire à onglet\n" +
            "scie circulaire evolution\n" +
            "scie à chantourner\n" +
            "scie à chantourner dremel\n" +
            "scie à chantourner professionnelle\n" +
            "scie à chantourner scheppach\n" +
            "scie à chantourner proxxon\n" +
            "scie à chantourner hegner\n" +
            "peluche géante\n" +
            "peluche géante pas cher\n" +
            "peluche géante ours\n" +
            "peluche géante disney\n" +
            "peluche géante licorne\n" +
            "peluche géante minion\n" +
            "peluche géante chien\n" +
            "épilateur lumière pulsée\n" +
            "epilateur pour le visage\n" +
            "épilateur maillot\n" +
            "épilateur électrique\n" +
            "rasoir electrique femme\n" +
            "creme depilatoire homme\n" +
            "epilateur sans douleur\n" +
            "tondeuse bikini\n" +
            "huile souchet\n" +
            "chauffe cire\n" +
            "creme depilatoire\n" +
            "huile de fourmi\n" +
            "épilateur sous l'eau\n" +
            "chatiere electronique\n" +
            "chatiere electronique sans collier\n" +
            "chatiere electronique avec collier\n" +
            "chatiere electronique sureflap\n" +
            "chatiere electronique petsafe\n" +
            "aspirateur silencieux\n" +
            "aspirateur silencieux sans sac\n" +
            "aspirateur silencieux rowenta\n" +
            "aspirateur silencieux pas cher\n" +
            "aspirateur silencieux avec sac\n" +
            "mijoteuse\n" +
            "mijoteuse electrique\n" +
            "mijoteuse kenwood\n" +
            "mijoteuse crock pot\n" +
            "mijoteuse seb\n" +
            "sechoir a linge\n" +
            "sechoir a linge pas cher\n" +
            "sechoir a linge exterieur\n" +
            "sechoir a linge interieur\n" +
            "trancheuse saucisson\n" +
            "trancheuse saucisson bois\n" +
            "trancheuse saucisson electrique\n" +
            "trancheuse saucisson pas cher\n" +
            "trancheuse saucisson manuelle\n" +
            "tricycle adulte\n" +
            "tricycle adulte electrique\n" +
            "tricycle adulte pas cher\n" +
            "tricycle adulte pliable\n" +
            "tricycle adulte electrique pas cher\n" +
            "essoreuse à salade\n" +
            "essoreuse à salade oxo\n" +
            "essoreuse à salade electrique\n" +
            "essoreuse à salade moulinex\n" +
            "essoreuse à salade zyliss\n" +
            "secateur electrique\n" +
            "secateur electrique professionnel\n" +
            "secateur electrique bosch\n" +
            "secateur electrique sans fil\n" +
            "coupe frite\n" +
            "coupe frite manuel\n" +
            "coupe frite electrique\n" +
            "coupe frite moulinex\n" +
            "coupe frite zyliss\n" +
            "ventilateur sur pied\n" +
            "ventilateur sur pied silencieux\n" +
            "ventilateur sur pied pas cher\n" +
            "ventilateur sur pied avec telecommande\n" +
            "ventilateur sur pied rowenta\n" +
            "porte bébé physiologique\n" +
            "porte bébé physiologique ergobaby\n" +
            "porte bébé physiologique babymoov\n" +
            "porte bébé physiologique pas cher\n" +
            "coupe branche\n" +
            "coupe branche fiskars\n" +
            "coupe branche gardena\n" +
            "perceuse a colonne\n" +
            "perceuse a colonne pas cher\n" +
            "perceuse a colonne fartools\n" +
            "perceuse a colonne einhell\n" +
            "perceuse a colonne bosch\n" +
            "pese bebe\n" +
            "pese bebe pas cher\n" +
            "pese bebe electronique\n" +
            "pese bebe terraillon\n" +
            "pese bebe chicco\n" +
            "chauffe eau instantané\n" +
            "chauffe eau instantané electrique\n" +
            "chauffe eau instantané gaz\n" +
            "machine à glaçons\n" +
            "machine à glaçons professionnelle\n" +
            "machine à glaçons pas cher\n" +
            "machine à glaçons koenig\n" +
            "friteuse sans huile\n" +
            "friteuse sans huile seb\n" +
            "friteuse sans huile pas cher\n" +
            "friteuse sans huile philips\n" +
            "hachoir a viande\n" +
            "hachoir a viande electrique\n" +
            "hachoir a viande professionnel\n" +
            "hachoir a viande manuel\n" +
            "hachoir a viande pas cher\n" +
            "batteur electrique\n" +
            "batteur electrique pas cher\n" +
            "batteur electrique moulinex\n" +
            "batteur electrique seb\n" +
            "batteur electrique bosch\n" +
            "percolateur\n" +
            "percolateur professionnel\n" +
            "percolateur pas cher\n" +
            "tire lait electrique\n" +
            "tire lait electrique pas cher\n" +
            "tire lait electrique medela\n" +
            "tire lait electrique tommee tippee\n" +
            "tire lait electrique tigex\n" +
            "lit pliant\n" +
            "lit pliant bébé\n" +
            "lit pliant adulte\n" +
            "lit pliant pas cher\n" +
            "lit pliant 1 place\n" +
            "aspirateur voiture\n" +
            "aspirateur voiture black et decker\n" +
            "mini aspirateur voiture\n" +
            "aspirateur voiture sans fil\n" +
            "aspirateur voiture pas cher\n" +
            "motobineuse electrique\n" +
            "motobineuse electrique pas cher\n" +
            "motobineuse electrique einhell\n" +
            "glaciere electrique\n" +
            "glaciere electrique pas cher\n" +
            "glaciere electrique campingaz\n" +
            "glaciere electrique camping\n" +
            "glaciere electrique mobicool\n" +
            "balancelle de jardin\n" +
            "balancelle de jardin pas cher\n" +
            "balancelle de jardin 3 places\n" +
            "balancelle de jardin en bois\n" +
            "balancelle de jardin 1 place\n" +
            "poussette trio\n" +
            "poussette trio bebe confort\n" +
            "poussette trio chicco\n" +
            "poussette trio loola\n" +
            "poussette trio peg perego\n" +
            "babyphone video\n" +
            "babyphone video pas cher\n" +
            "babyphone video sans fil\n" +
            "babyphone video vtech\n" +
            "babyphone video avent\n" +
            "balancelle bébé\n" +
            "balancelle bébé pas cher\n" +
            "balancelle bébé electrique\n" +
            "balancelle bébé babymoov\n" +
            "balancelle bébé fisher price\n" +
            "souffleur thermique\n" +
            "souffleur thermique pas cher\n" +
            "souffleur thermique ryobi\n" +
            "ventilateur silencieux\n" +
            "ventilateur silencieux sur pied\n" +
            "ventilateur silencieux pas cher\n" +
            "machine a pop corn\n" +
            "machine a pop corn pas cher\n" +
            "machine a pop corn professionnel\n" +
            "mini machine a pop corn\n" +
            "machine a pop corn simeo\n" +
            "tuyau arrosage extensible\n" +
            "tuyau arrosage extensible 30m\n" +
            "tuyau arrosage extensible 15m\n" +
            "tuyau arrosage extensible professionnel\n" +
            "theiere electrique\n" +
            "theiere electrique tefal\n" +
            "bouilloire theiere electrique\n" +
            "inhalateur\n" +
            "inhalateur electrique\n" +
            "inhalateur asthme\n" +
            "inhalateur electrique avec masque\n" +
            "tondeuse manuelle\n" +
            "tondeuse manuelle pas cher\n" +
            "tondeuse manuelle à lame hélicoïdale\n" +
            "aspirateur de table\n" +
            "aspirateur de table pas cher\n" +
            "aspirateur de table sans fil\n" +
            "souffleur thermique\n" +
            "souffleur thermique pas cher\n" +
            "souffleur thermique echo\n" +
            "souffleur thermique ryobi\n" +
            "aspirateur a cendre\n" +
            "aspirateur a cendre karcher\n" +
            "aspirateur a cendre pas cher\n" +
            "filtre pour aspirateur a cendre\n" +
            "aspirateur a cendre professionnel\n" +
            "ventilateur pas cher\n" +
            "dyson ventilateur pas cher\n" +
            "lustre ventilateur pas cher\n" +
            "colonne ventilateur pas cher\n" +
            "ventilateur pas cher sur pied\n" +
            "appareil photo enfant\n" +
            "appareil photo enfant vtech\n" +
            "appareil photo enfant pas cher\n" +
            "pluviomètre\n" +
            "pluviomètre electronique\n" +
            "pluviomètre sans fil\n" +
            "pluviomètre professionnel\n" +
            "relieuse\n" +
            "relieuse spirale\n" +
            "relieuse pas cher\n" +
            "perforelieuse\n" +
            "relieuse professionnelle\n" +
            "cocotte en fonte\n" +
            "cocotte en fonte pas cher\n" +
            "cocotte en fonte le creuset\n" +
            "cocotte en fonte invicta\n" +
            "cocotte en fonte staub\n" +
            "scie radiale\n" +
            "scie radiale metabo\n" +
            "scie radiale makita\n" +
            "scie radiale bosch\n" +
            "scie radiale dewalt\n" +
            "chaussure bateau\n" +
            "chaussure bateau homme\n" +
            "chaussure bateau femme\n" +
            "chaussure bateau tbs\n" +
            "chaussure bateau timberland\n" +
            "sterilisateur biberon\n" +
            "sterilisateur biberon electrique\n" +
            "sterilisateur biberon pas cher\n" +
            "sterilisateur biberon bebe confort\n" +
            "vasque à poser\n" +
            "vasque à poser rectangulaire\n" +
            "vasque à poser pas cher\n" +
            "vasque à poser ronde\n" +
            "vasque à poser ceramique\n" +
            "arbre a chat\n" +
            "arbre a chat pas cher\n" +
            "arbre a chat geant\n" +
            "arbre a chat bois\n" +
            "patin a roulette\n" +
            "patin a roulette fille\n" +
            "patin a roulette soy luna\n" +
            "patin a roulette garcon\n" +
            "patin a roulette rio\n" +
            "brosse lissante\n" +
            "brosse lissante pas cher\n" +
            "babyliss brosse lissante\n" +
            "brosse lissante professionnelle\n" +
            "brosse lissante philips\n" +
            "poele induction\n" +
            "poele induction pas cher\n" +
            "poele induction inox\n" +
            "poele induction ceramique\n" +
            "poele induction tefal\n" +
            "ponceuse a bande\n" +
            "ponceuse a bande pas cher\n" +
            "ponceuse a bande makita\n" +
            "broyeur de branches\n" +
            "broyeur de branches electrique\n" +
            "broyeur de branches pas cher\n" +
            "casque moto cross\n" +
            "casque moto cross pas cher\n" +
            "casque moto cross avec visiere\n" +
            "casque moto cross avec lunette\n" +
            "rehausseur de chaise\n" +
            "rehausseur de chaise pas cher\n" +
            "rehausseur de chaise babymoov\n" +
            "niche pour chien\n" +
            "niche pour chien pas cher\n" +
            "niche pour chien en bois\n" +
            "niche pour chien xl\n" +
            "pompe vide cave\n" +
            "pompe vide cave pas cher\n" +
            "pompe vide cave eau chargée\n" +
            "appareil de musculation\n" +
            "appareil de musculation complet\n" +
            "appareil de musculation pas cher\n" +
            "appareil de musculation multifonction\n" +
            "ponceuse\n" +
            "ponceuse a bande\n" +
            "ponceuse bosch\n" +
            "ponceuse excentrique\n" +
            "ponceuse girafe";
}
