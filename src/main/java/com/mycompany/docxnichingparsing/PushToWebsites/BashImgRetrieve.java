package com.mycompany.docxnichingparsing.PushToWebsites;

import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.HashMap;

public class BashImgRetrieve {
    private static String DB_NAME="storeamazon3";
    private static String DB_HOST="localhost";
    private static String DB_USER="root";
    private static String DB_PASS="6#hW^Ockal2e7@oYckcIxBmFL&5B*";
    private static Connection conn;
    private static Connection conn0;
    private static String login="adminLaMif";
    private static String password="3t6qNzPLRam2!bb0X0Jommzc";

    public static void init() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        conn = DriverManager.getConnection(
                "jdbc:mysql://" + DB_HOST + ":3306/" + DB_NAME + "?serverTimezone=UTC&useSSL=false", DB_USER, DB_PASS);

    }
    public static void main(String[] args) throws SQLException, ClassNotFoundException, IOException {
        init();
        //imgRetrieve();
        //inputASINRetrieve();
        //inputTermsName();
        //inputGuideCompaImages();
        retrieveCompaAsin();
    }
    private static void retrieveCompaAsin() throws IOException, SQLException {
        StringBuilder output=new StringBuilder();
        String query =
                "select asin,domain,id\n" +
                        "from storecorrespproductreviews s";
        PreparedStatement stmt = conn.prepareStatement(query);
        HashMap<String,String> h=new HashMap<>();
        try {
            ResultSet rs4 = stmt.executeQuery(query);
            while (rs4.next()) {
                //System.out.println(rs4.getString(1)+" | "+rs4.getString(2));
                String asin = rs4.getString(1);
                String domain = rs4.getString(2);
                Integer postId = Integer.parseInt(rs4.getString(3));
                int compaId=postId-(postId%20);
                //output.append("cd /root/websites/"+domain+"\n");
                //output.append("wp post meta update "+id+" asin '"+asin+"' --allow-root\n");
                //System.out.println(domain +" "+compaId+ " "+asin);
                String val=h.get(domain+";"+compaId);
                if(val==null){
                    h.put(domain+";"+compaId,asin);
                }else{
                    h.put(domain+";"+compaId,h.get(domain+";"+compaId)+";"+asin);
                }
                System.out.println(h.get(domain+";"+compaId));
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        FileUtils.writeStringToFile(new File("P:\\Dropbox\\work\\scripts\\niche-scripts\\setAsin.sh"), output.toString());
    }

    private static void inputTermsName() throws IOException {
        String[] tabName=Constants.guideName.split("\n");
        String[] tabNum=Constants.guideNumber.split("\n");
        String[] domains=Constants.domains.split("\n");
        StringBuilder output=new StringBuilder();
        int cour=0;
        for(int i=0;i<522;i++){
            Integer categId=50+(Integer.parseInt(tabNum[i])-1)%1000 ;
            output.append("cd /root/niche-websites/"+domains[i]+"\n");
            output.append("wp term update category "+categId+" --name='"+tabName[i]+"' --allow-root \n");
            cour=categId;
        }
        System.out.println(output);
        FileUtils.writeStringToFile(new File("P:\\Dropbox\\work\\scripts\\niche-scripts\\setCateg.sh"), output.toString());

    }

    private static void inputGuideCompaImages() throws IOException, SQLException {
        StringBuilder output=new StringBuilder();
        String query =
                "select asin,domain,id\n" +
                        "from storecorrespproductreviews s";
        PreparedStatement stmt = conn.prepareStatement(query);
        HashMap<String,Integer> h=new HashMap<>();
        try {
            ResultSet rs4 = stmt.executeQuery(query);
            while (rs4.next()) {
                //System.out.println(rs4.getString(1)+" | "+rs4.getString(2));
                String asin = rs4.getString(1);
                String domain = rs4.getString(2);
                String id = rs4.getString(3);
                int categId=Integer.parseInt(id)%20;
                System.out.println(categId+"--"+asin);
                //output.append("cd /root/websites/"+domain+"\n");
                //output.append("wp post meta update "+id+" asin '"+asin+"' --allow-root\n");
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        FileUtils.writeStringToFile(new File("P:\\Dropbox\\work\\scripts\\niche-scripts\\setAsin.sh"), output.toString());
    }
    private static void inputASINRetrieve() throws IOException, SQLException {
        StringBuilder output=new StringBuilder();
        String query =
                "select asin,domain,id\n" +
                        "from storecorrespproductreviews s";
        PreparedStatement stmt = conn.prepareStatement(query);
        HashMap<String,Integer> h=new HashMap<>();
        try {
            ResultSet rs4 = stmt.executeQuery(query);
            while (rs4.next()) {
                //System.out.println(rs4.getString(1)+" | "+rs4.getString(2));
                String asin = rs4.getString(1);
                String domain = rs4.getString(2);
                String id = rs4.getString(3);
                output.append("cd /root/websites/"+domain+"\n");
                output.append("wp post meta update "+id+" asin '"+asin+"' --allow-root\n");
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        FileUtils.writeStringToFile(new File("P:\\Dropbox\\work\\scripts\\niche-scripts\\setAsin.sh"), output.toString());
    }

    public static void imgRetrieve() throws SQLException, IOException {
        StringBuilder output=new StringBuilder();
        String query =
                "select distinct(s.asin),s.images\n" +
                        "from storepages s";
        PreparedStatement stmt = conn.prepareStatement(query);
        HashMap<String,Integer> h=new HashMap<>();
        try {
            ResultSet rs4 = stmt.executeQuery(query);
            int cour = 0;
            int prevId = 0;
            while (rs4.next()) {
                //System.out.println(rs4.getString(1)+" | "+rs4.getString(2));
                String asin=rs4.getString(1);
                if(rs4.getString(2)==null)
                    continue;

                String[] images = rs4.getString(2).split("\\|");
                int count=0;
                for(String url:images){
                    if(count==0){
                        output.append("\n"+"mkdir /root/websites/" +
                                "template-niche.pbconseil.ovh/wp-content/" +
                                "uploads/images/"+asin);
                        System.out.println("mkdir /root/websites/" +
                                "template-niche.pbconseil.ovh/wp-content/" +
                                "uploads/images/"+asin);
                    }
                    output.append("\n"+"wget -O /root/websites/" +
                            "template-niche.pbconseil.ovh/wp-content/" +
                            "uploads/images/"+asin+"/img-"+count+".jpg "+url);
                    System.out.println("wget -O /root/websites/" +
                            "template-niche.pbconseil.ovh/wp-content/" +
                            "uploads/images/"+asin+"/img-"+count+".jpg "+url);

                    //System.out.println(asin+" "+images.length);
                    count++;
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        FileUtils.writeStringToFile(new File("output.txt"), output.toString());
    }
}
