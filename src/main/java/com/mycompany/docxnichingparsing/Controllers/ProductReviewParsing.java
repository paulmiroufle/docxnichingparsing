/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.docxnichingparsing.Controllers;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.xwpf.converter.core.FileURIResolver;
import org.apache.poi.xwpf.converter.xhtml.XHTMLConverter;
import org.apache.poi.xwpf.converter.xhtml.XHTMLOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.mycompany.docxnichingparsing.Model.ProductComparison;
import com.mycompany.docxnichingparsing.Model.ProductReview;
import com.mysql.cj.jdbc.Driver;

/**
 *
 * @author Administrator
 */
public class ProductReviewParsing {
	private static String test="salut ca va?";
	private static String DB_NAME="storeamazon";
	private static String DB_HOST="localhost";
	private static String DB_USER="root";
	private static String DB_PASS="6#hW^Ockal2e7@oYckcIxBmFL&5B*";
	private static Connection conn;
	private static Statement stmt;
	private static String keyword;

	public static void init() throws SQLException, ClassNotFoundException {
		Class.forName("com.mysql.cj.jdbc.Driver");
		conn = DriverManager.getConnection(
				"jdbc:mysql://" + DB_HOST + ":3306/" + DB_NAME + "?serverTimezone=UTC&useSSL=false&useUnicode=true", DB_USER, DB_PASS);
		String query = "SET NAMES 'utf8mb4'";
		Statement stmt = conn.createStatement();
		((Statement) stmt).executeQuery(query);
	}
	public static void close() throws SQLException{
		conn.close();
	}
	public static void main(String[] args) throws Exception {
		init();
		List<String> asins=new ArrayList<>();
		List<String> metaDesc=new ArrayList<>();
		List<ProductReview> resultList = new ArrayList<>();
		InputStream in;
		// create a file that is really a directory
		File aDirectory = new File("C:\\Users\\Lbedoucha Conseil\\Dropbox\\work" +
				"\\Niching\\fiches produit\\vague finale poste correction liantsoa\\vague 60 90");

		// get a listing of all files in the directory
		File[] filesInDir = aDirectory.listFiles();

		// sort the list of files (optional)
		// Arrays.sort(filesInDir);

		// have everything i need, just print it now
		for ( int i=0; i<filesInDir.length; i++ ) {
			//System.out.println(filesInDir[i].getName());
			if(!filesInDir[i].getName().endsWith(".docx")|filesInDir[i].getName().startsWith("~$"))
				continue;
			try {
				in = new FileInputStream(filesInDir[i]);//new File("4 Fiches produit machine a pain.docx"));
				XWPFDocument document = new XWPFDocument(in);
				// using XWPFWordExtractor Class
				// XWPFWordExtractor xdoc = new XWPFWordExtractor(document);
				XHTMLOptions options = XHTMLOptions.create().URIResolver(new FileURIResolver(new File("word/media")));

				OutputStream out = new ByteArrayOutputStream();

				XHTMLConverter.getInstance().convert(document, out, options);
				String HTMLSTring = out.toString();
				Document html = Jsoup.parse(HTMLSTring);

				Elements links = html.select("p");

				List<List<Element>> productReviews = new ArrayList<>();
				List<Element> products = null;
				for (Element link : links) {
					// System.out.println(link.text().equals("Coupe Bordure : Guide
					// Comparatif"));
					if (link.text().trim().startsWith("Fiches produits") || link.text().trim().startsWith("Fiche produit")
					|| link.text().trim().startsWith("Fiches produit")|| link.text().trim().startsWith("Fiche de produit") ||
							link.toString().contains("<span class=\"Normal Titre1\">Fiche")) {
						products = new ArrayList<>();
						productReviews.add(products);
					}
					if (!link.text().trim().isEmpty()){
						if(products==null){
							System.out.println("error --> "+filesInDir[i]);
						}else {
							products.add(link);
						}
					}

				}
				if(productReviews.size()<9)
					System.out.println(filesInDir[i].getName()+"-->"+productReviews.size());
				for (List<Element> elements : productReviews) {
					ProductReview productReview = new ProductReview();
					resultList.add(productReview);
					Iterator<Element> itr = elements.iterator();
					String key = itr.next().text();
					productReview.setASIN(AsinGetter.asinGetter(key));//.substring(key.indexOf(":"), key.indexOf("-")));
					if(!AsinGetter.asinGetter(key).startsWith("B")){
						System.out.println("**PB**** "+filesInDir[i]+" -- "+key);
					}
					productReview.setFirstTitle(itr.next().toString());
					String title2 = itr.next().toString();
					title2 = title2.replaceFirst("<p class=\"Normal Titre2\" style=\"text-align:justified;\">", "<h2>");
					if (title2.contains("<p class=\"Normal Titre2\">")) {
						title2 = title2.replaceFirst("<p class=\"Normal Titre2\">", "<h2>");
					}
					title2 = title2.replace("</p>", "</h2>");
					productReview.setSecondTitle(title2);
					itr.next();
					productReview.setMetaDescription(itr.next().toString());
					boolean isDone = false;
					itr.next();
					do {
						if (!isDone) {
							Element element = itr.next();
							//if (!element.text().trim().replaceAll("\\W", "").equalsIgnoreCase("nombredevoies")) {
							if (!element.toString().startsWith("<p class=\"Normal Titre2\"")) {
								String introString=productReview.getIntro() + element.toString();
								if(introString.length()<5){
									System.out.println("**** problem ***** "  + filesInDir[i]);
								}
								productReview.setIntro(introString);
							} else {
								productReview.setMainText(element.toString());
								isDone = true;

							}
						}
					} while (itr.hasNext() && !isDone);
					isDone = false;
					do {
						Element element = itr.next();
						if (!element.text().trim().replaceAll("\\W", "").equalsIgnoreCase("Avantages")) {
							productReview.setMainText(productReview.getMainText() + element.toString());
						} else {
							isDone = true;

						}
					} while (itr.hasNext() && !isDone);
					isDone = false;
					productReview.setAdvantages("");
					do {
						//System.out.println(productReview.getFirstTitle());
						Element element = itr.next();
						if (!element.text().trim().replaceAll("\\W", "").equalsIgnoreCase("Inconvnients")) {
							productReview.setAdvantages(productReview.getAdvantages() + element.toString());
						} else {
							isDone = true;

						}
					} while (itr.hasNext() && !isDone);
					isDone = false;
					productReview.setDrawbacks("");
				/*while(itr.hasNext()) {
					Element element = itr.next();
					productReview.setDrawbacks(productReview.getDrawbacks() + element.toString());
				}*/

					do {
						if (!isDone) {
							//System.out.println(Jsoup.parse(productReview.getMetaDescription()).text());
							Element element = itr.next();
							//if (!element.text().trim().replaceAll("\\W", "").equalsIgnoreCase("nombredevoies")) {
							if (!element.toString().startsWith("<p class=\"Normal Titre2\"")) {
								productReview.setDrawbacks(productReview.getDrawbacks() + element.toString());
							} else {
								isDone = true;
							}
						}

					} while (itr.hasNext() && !isDone);
					if(!metaDesc.contains(Jsoup.parse(productReview.getMetaDescription()).text())) {
						System.out.println("*** ON insert en DB"+Jsoup.parse(productReview.getFirstTitle()).text());
						//DbInserter.insertInDb(productReview, conn, getCount(asins, productReview.getASIN()));
						asins.add(productReview.getASIN());
						metaDesc.add(Jsoup.parse(productReview.getMetaDescription()).text());
					}else{
						System.out.println("esquive "+productReview.getASIN());
					}
				}
				resultList.forEach(model -> {
					print(model);
				});



			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("error"+filesInDir[i]);
				//e.printStackTrace();
			}

		}

	}
	private static int getCount(List<String> asins,String asin){
		int ret=0;
		for(String cour:asins){
			if(cour.contains(asin)){
				ret++;
			}
		}
		return ret;
	}
	private static void print(ProductReview model) {
		if(!model.getASIN().startsWith("B")){
			System.out.println("*** PROBLEM ASIN ****"+Jsoup.parse(model.getFirstTitle()).text());
			System.out.println(model.getASIN());
		}
		if(model.getIntro().length()<5){
			System.out.println("*** PROBLEM INTRO ****"+Jsoup.parse(model.getFirstTitle()).text());
		}
		if(model.getAdvantages().length()<5){
			System.out.println("*** PROBLEM ADV ****");
		}
		if(model.getDrawbacks().length()<5){
			System.out.println("*** PROBLEM DRAW ****"+Jsoup.parse(model.getFirstTitle()).text());
		}
		if(model.getMainText().length()<5){
			System.out.println("*** PROBLEM MAIN ****");
		}
		if(model.getFirstTitle().length()<5){
			System.out.println("*** PROBLEM TITLE1 ****");
		}
		if(model.getMetaDescription().length()<5){
			System.out.println("*** PROBLEM MetaDesc ****");
		}
		if(model.getSecondTitle().length()<5){
			System.out.println("*** PROBLEM TITLE2 ****");
		}
		if(!model.getMainText().contains("avis")){
			System.out.println("*** PROBLEM notre avis ****"+Jsoup.parse(model.getFirstTitle()).text());
		}
		/*
		System.out.println("getASIN: " + model.getASIN());
		System.out.println("getFirstTitle: " + model.getFirstTitle());
		System.out.println("getSecondTitle: " + model.getSecondTitle());
		System.out.println("getMetaDescription: " + model.getMetaDescription());
		System.out.println("getIntro: " + model.getIntro());
		System.out.println("getMainText: " + model.getMainText());
		System.out.println("getAdvantages: " + model.getAdvantages());
		System.out.println("getDrawbacks: " + model.getDrawbacks());
		System.out.println("------------------------------------------------------------------------------------------------------------------------------------------");
		*/

	}

}
