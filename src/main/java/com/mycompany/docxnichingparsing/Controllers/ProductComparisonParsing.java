/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.docxnichingparsing.Controllers;

import com.mycompany.docxnichingparsing.Model.BuyingAdviceTitle;
import com.mycompany.docxnichingparsing.Model.Criteria;
import com.mycompany.docxnichingparsing.Model.InnerReview;
import com.mycompany.docxnichingparsing.Model.ProductComparison;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.xwpf.converter.core.FileURIResolver;
import org.apache.poi.xwpf.converter.core.XWPFConverterException;
import org.apache.poi.xwpf.converter.xhtml.XHTMLConverter;
import org.apache.poi.xwpf.converter.xhtml.XHTMLOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * @author Administrator
 */
public class ProductComparisonParsing {

	public static void main(String[] args) throws XWPFConverterException, IOException {
		startParsing();

	}

	private static void startParsing() throws FileNotFoundException, IOException {
		List<ProductComparison> resultList = new ArrayList<>();
		// convert .docx to HTML string
		InputStream in = new FileInputStream(new File("productComparison.docx"));
		XWPFDocument document = new XWPFDocument(in);
		// using XWPFWordExtractor Class
		// XWPFWordExtractor xdoc = new XWPFWordExtractor(document);
		XHTMLOptions options = XHTMLOptions.create().URIResolver(new FileURIResolver(new File("word/media")));

		OutputStream out = new ByteArrayOutputStream();

		XHTMLConverter.getInstance().convert(document, out, options);
		String HTMLSTring = out.toString();
		// System.out.println(HTMLSTring);
		Document html = Jsoup.parse(HTMLSTring);

		Elements links = html.select("p");
		List<List<Element>> productComparisons = new ArrayList<>();
		List<Element> products = null;
		for (Element link : links) {
			// System.out.println(link.text().equals("Coupe Bordure : Guide
			// Comparatif"));
			if (link.text().trim().startsWith("Coupe Bordure : Guide Comparatif")) {
				products = new ArrayList<>();
				productComparisons.add(products);
			}
			products.add(link);
			/*
			 * if(link.childNodeSize() >0)
			 * System.out.println(link.childNode(0).toString());
			 */
		}

		Iterator<List<Element>> productComparisonItr = productComparisons.iterator();
		while(productComparisonItr.hasNext()) {
			List<Element> productComparisonNode = productComparisonItr.next();
		ProductComparison productComparison = new ProductComparison();
		setKeyword(productComparison, productComparisonNode);
		 setShortTitle(productComparison, productComparisonNode);
		 setLongTitle(productComparison, productComparisonNode);
		 setMetaDesc(productComparison, productComparisonNode);
		 setIntro(productComparison, productComparisonNode);
		List<List<Element>> listInnerReviews = listInnerReviews(productComparisonNode);
		for (List<Element> inners : listInnerReviews) {
			InnerReview innerReview = new InnerReview();
			Iterator<Element> itr = inners.iterator();
			innerReview.setInnerReviewTitle(itr.next().toString());
			itr.next().toString();
			innerReview.setSection1(itr.next().toString());
			itr.next().toString();
			innerReview.setSection2(itr.next().toString());
			itr.next().toString();
			innerReview.setSection3(itr.next().toString());
			productComparison.getInnerReviewList().add(innerReview);
		}
		BuyingAdviceTitle buyingAdviceTitle = new BuyingAdviceTitle();
		productComparison.setBuyingAdviceTitle(buyingAdviceTitle);
		List<Element> buyingAdviceTitles = listBuyingAdviceTitle(productComparisonNode, buyingAdviceTitle);
		Iterator<Element> buyingAdviceTitleItr = buyingAdviceTitles.iterator();
		buyingAdviceTitleItr.next();
		buyingAdviceTitle.setBuyingAdviceTitle3(buyingAdviceTitleItr.next().toString());
		buyingAdviceTitle.setCriteriasTitle(buyingAdviceTitleItr.next().toString());

		while (buyingAdviceTitleItr.hasNext()) {
			Element node = buyingAdviceTitleItr.next();
			if (node.text().trim().replaceAll("\\W", "").toLowerCase().startsWith("critre")) {
				buyingAdviceTitle.getCriterias().add(new Criteria(node.toString(), buyingAdviceTitleItr.next().toString()));
			}
		}
		printProductComparison(productComparison);
		}
		
	}

	private static void printProductComparison(ProductComparison productComparison) {
		System.out.println("getKeyWord: " + productComparison.getKeyWord());
		System.out.println("getShortTitle: " + productComparison.getShortTitle());
		System.out.println("getLongTitle: " + productComparison.getLongTitle());
		System.out.println("getMetaDescription: " + productComparison.getMetaDescription());
		System.out.println("getIntro: " + productComparison.getIntro());
		
		productComparison.getInnerReviewList().forEach(model -> {
			System.out.println("getInnerReviewTitle: " + model.getInnerReviewTitle());
			System.out.println("getSection1: " + model.getSection1());
			System.out.println("getSection2: " + model.getSection2());
			System.out.println("getSection3: " + model.getSection3());
		});
		System.out.println("getBuyingAdviceTitle2: " +productComparison.getBuyingAdviceTitle().getBuyingAdviceTitle2());
		System.out.println("getBuyingAdviceTitle3: " +productComparison.getBuyingAdviceTitle().getBuyingAdviceTitle3());
		System.out.println("getCriteriasTitle: " +productComparison.getBuyingAdviceTitle().getCriteriasTitle());
		productComparison.getBuyingAdviceTitle().getCriterias().forEach(model -> {
			System.out.println("getTitle: " + model.getTitle());
			System.out.println("getText: " + model.getText());
		});
		System.out.println("------------------------------------------------------------------------------------------------------------------------------------------");
	}

	private static List<Element> listBuyingAdviceTitle(List<Element> nodes, BuyingAdviceTitle model) {
		boolean letStart = false;
		List<Element> results = null;
		for (Element node : nodes) {
			if (node.text().trim().replaceAll("\\W", "").toLowerCase().startsWith("nosconseils")) {
				letStart = true;
				results = new ArrayList<>();
				model.setBuyingAdviceTitle2(node.toString());
				continue;
			}
			if (letStart && !node.text().trim().isEmpty()) {
				results.add(node);
			}
		}
		return results;
	}

	private static void setKeyword(ProductComparison productComparison, List<Element> nodes) {
		productComparison.setKeyWord(nodes.get(0).toString().substring(0,nodes.get(0).toString().indexOf(":")));

	}


	private static List<List<Element>> listInnerReviews(List<Element> nodes) {
		boolean letStart = false;
		List<List<Element>> results = new ArrayList<>();
		List<Element> innerReview = null;
		for (Element node : nodes) {
			if (node.text().trim().replaceAll("\\W", "").startsWith("B009NH6NR0")
					|| node.text().trim().replaceAll("\\W", "").startsWith("B00MA3TH2M")) {
				letStart = true;
				innerReview = new ArrayList<>();
				results.add(innerReview);

			}
			if (node.text().trim().replaceAll("\\W", "").toLowerCase().startsWith("nosconseils")) {
				break;
			}
			if (letStart && !node.text().trim().isEmpty()) {
				innerReview.add(node);
			}
		}
		return results;
	}

	private static void setIntro(ProductComparison productComparison, List<Element> nodes) {
		StringBuilder sb = new StringBuilder();
		boolean letStart = false;

		for (Element node : nodes) {
			if ("Intro".equalsIgnoreCase(node.text().trim().replaceAll("\\W", ""))) {
				letStart = true;
				continue;
			}
			if (node.text().trim().replaceAll("\\W", "").startsWith("B009NH6NR0")
					|| node.text().trim().replaceAll("\\W", "").startsWith("B00MA3TH2M")) {
				break;
			}
			if (letStart && !node.text().trim().isEmpty()) {
				sb.append(node.toString());
			}
		}
		productComparison.setIntro(sb.toString());
	}

	private static void setShortTitle(ProductComparison productComparison, List<Element> nodes) {
		StringBuilder sb = new StringBuilder();
		boolean letStart = false;
		for (Element node : nodes) {
			// System.out.println(node.text());
			if ("Titrecourtcomparatif".equalsIgnoreCase(node.text().trim().replaceAll("\\W", ""))) {
				letStart = true;
				continue;
			}
			if ("Titrelongcomparatif".equalsIgnoreCase(node.text().trim().replaceAll("\\W", ""))) {
				break;
			}
			if (letStart && !node.text().trim().isEmpty()) {
				sb.append(node.toString());
			}
		}
		productComparison.setShortTitle(sb.toString());
	}

	private static void setLongTitle(ProductComparison productComparison, List<Element> nodes) {
		StringBuilder sb = new StringBuilder();
		boolean letStart = false;
		for (Element node : nodes) {
			if ("Titrelongcomparatif".equalsIgnoreCase(node.text().trim().replaceAll("\\W", ""))) {
				letStart = true;
				continue;
			}
			String text = node.text().trim().replaceAll("\\W", "");
			if (text.equalsIgnoreCase("MetaDescription")) {
				break;
			}
			if (letStart && !node.text().trim().isEmpty()) {
				sb.append(node.toString());
			}
		}
		productComparison.setLongTitle(sb.toString());
	}

	private static void setMetaDesc(ProductComparison productComparison, List<Element> nodes) {
		StringBuilder sb = new StringBuilder();
		boolean letStart = false;
		for (Element node : nodes) {
			// System.out.println(node.text());
			if ("MetaDescription".equalsIgnoreCase(node.text().trim().replaceAll("\\W", ""))) {
				letStart = true;
				continue;
			}
			if ("Intro".equalsIgnoreCase(node.text().trim().replaceAll("\\W", ""))) {
				break;
			}
			if (letStart && !node.text().trim().isEmpty()) {
				sb.append(node.toString());
			}
		}
		productComparison.setMetaDescription(sb.toString());
	}
}
