package com.mycompany.docxnichingparsing.Controllers;

import com.mycompany.docxnichingparsing.Model.ProductReview;
import org.jsoup.Jsoup;

import java.sql.Connection;
import java.sql.PreparedStatement;

import static com.mycompany.docxnichingparsing.Controllers.MySQLUtils.mysql_real_escape_string;

public class DbInserter {

    public static void insertInDb(ProductReview model, Connection conn, int num) throws Exception {
        String query="insert into storeproductreviews" +
                "(ASIN,firstTitle,secondTitle,metaDescription,intro,mainText,advantages,drawbacks,num)" +
                "values" +
                "(?,?,?,?,?,?,?,?,?)";

        //System.out.println(Jsoup.parse(model.getFirstTitle()).text());
        PreparedStatement ps=conn.prepareStatement(query);
        ps.setString(1, model.getASIN());
        ps.setString(2, model.getFirstTitle());
        ps.setString(3, model.getSecondTitle());
        String metaDesc=Jsoup.parse(model.getMetaDescription()).text().substring(3);
        if(metaDesc.startsWith("modèle")|metaDesc.startsWith("modele")){
            metaDesc="Ce "+metaDesc;
        }
        ps.setString(4, metaDesc);
        ps.setString(5, model.getIntro());
        ps.setString(6, model.getMainText());
        ps.setString(7, model.getAdvantages());
        ps.setString(8, model.getDrawbacks());
        ps.setInt(9, num);
        //System.out.println(html);
        int p=ps.executeUpdate();
    }

}
