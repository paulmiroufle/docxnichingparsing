package com.mycompany.docxnichingparsing.Model;


public class InnerReview{
    String innerReviewTitle;
    String section1;
    String section2;
    String section3;


    

	public String getInnerReviewTitle() {
		return innerReviewTitle;
	}


	public void setInnerReviewTitle(String innerReviewTitle) {
		this.innerReviewTitle = innerReviewTitle;
	}


	public String getSection1() {
		return section1;
	}


	public void setSection1(String section1) {
		this.section1 = section1;
	}


	public String getSection2() {
		return section2;
	}


	public void setSection2(String section2) {
		this.section2 = section2;
	}


	public String getSection3() {
		return section3;
	}


	public void setSection3(String section3) {
		this.section3 = section3;
	}

}