package docxnichingparsing2.Model;

import java.util.ArrayList;
import java.util.List;

public class BuyingAdviceTitle {

	private String buyingAdviceTitle2;
	private String buyingAdviceTitle3;
	private String criteriasTitle;
	private List<Criteria> criterias = new ArrayList<Criteria>();
	public String getBuyingAdviceTitle2() {
		return buyingAdviceTitle2;
	}
	public void setBuyingAdviceTitle2(String buyingAdviceTitle2) {
		this.buyingAdviceTitle2 = buyingAdviceTitle2;
	}
	public String getBuyingAdviceTitle3() {
		return buyingAdviceTitle3;
	}
	public void setBuyingAdviceTitle3(String buyingAdviceTitle3) {
		this.buyingAdviceTitle3 = buyingAdviceTitle3;
	}
	public String getCriteriasTitle() {
		return criteriasTitle;
	}
	public void setCriteriasTitle(String criteriasTitle) {
		this.criteriasTitle = criteriasTitle;
	}
	public List<Criteria> getCriterias() {
		return criterias;
	}
	public void setCriterias(List<Criteria> criterias) {
		this.criterias = criterias;
	}
	
}
