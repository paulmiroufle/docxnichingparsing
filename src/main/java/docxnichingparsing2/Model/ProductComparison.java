package docxnichingparsing2.Model;

import java.util.ArrayList;
import java.util.List;

public class ProductComparison {
private String keyWord;
private String shortTitle;
private String longTitle;
private String metaDescription;
private String intro;
private List<InnerReview> innerReviewList = new ArrayList<>();
private BuyingAdviceTitle buyingAdviceTitle;
public String getKeyWord() {
	return keyWord;
}
public void setKeyWord(String keyWord) {
	this.keyWord = keyWord;
}
public String getShortTitle() {
	return shortTitle;
}
public void setShortTitle(String shortTitle) {
	this.shortTitle = shortTitle;
}
public String getLongTitle() {
	return longTitle;
}
public void setLongTitle(String longTitle) {
	this.longTitle = longTitle;
}
public String getMetaDescription() {
	return metaDescription;
}
public void setMetaDescription(String metaDescription) {
	this.metaDescription = metaDescription;
}
public String getIntro() {
	return intro;
}
public void setIntro(String intro) {
	this.intro = intro;
}

public BuyingAdviceTitle getBuyingAdviceTitle() {
	return buyingAdviceTitle;
}
public void setBuyingAdviceTitle(BuyingAdviceTitle buyingAdviceTitle) {
	this.buyingAdviceTitle = buyingAdviceTitle;
}
public List<InnerReview> getInnerReviewList() {
	return innerReviewList;
}
public void setInnerReviewList(List<InnerReview> innerReviewList) {
	this.innerReviewList = innerReviewList;
}
}
