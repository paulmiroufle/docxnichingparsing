package docxnichingparsing2.Controllers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AsinGetter {
    public static String asinGetter(String input){
        String rx = "([a-zA-Z0-9]{10})";
        Pattern p = Pattern.compile(rx);
        Matcher matcher = p.matcher(input);
        if (matcher.find()){
            return matcher.group();
        }else{
            return "";
        }
    }
    public static void main(String[] args){
        System.out.println(asinGetter("Fiche produit 7 : B00GMV5HDG - Chicco - 00005577000000 - Pèse Bébé Electronique"));
    }
}
