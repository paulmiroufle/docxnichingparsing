/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package docxnichingparsing2.Controllers;

import com.mycompany.docxnichingparsing.Controllers.DbInserter;

import com.mycompany.docxnichingparsing.Model.ProductReview;
import org.apache.poi.xwpf.converter.core.FileURIResolver;
import org.apache.poi.xwpf.converter.xhtml.XHTMLConverter;
import org.apache.poi.xwpf.converter.xhtml.XHTMLOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class ProductReviewParsing {private static String test="salut ca va?";
	private static String DB_NAME="storeamazon";
	private static String DB_HOST="localhost";
	private static String DB_USER="root";
	private static String DB_PASS="6#hW^Ockal2e7@oYckcIxBmFL&5B*";
	private static Connection conn;
	private static Statement stmt;
	private static String keyword;

	public static void init() throws SQLException, ClassNotFoundException {
		Class.forName("com.mysql.cj.jdbc.Driver");
		conn = DriverManager.getConnection(
				"jdbc:mysql://" + DB_HOST + ":3306/" + DB_NAME + "?serverTimezone=UTC&useSSL=false&useUnicode=true", DB_USER, DB_PASS);
		String query = "SET NAMES 'utf8mb4'";
		Statement stmt = conn.createStatement();
		((Statement) stmt).executeQuery(query);
	}
	public static void close() throws SQLException{
		conn.close();
	}
	public static void main(String[] args) throws Exception {
		init();
		List<String> asins=new ArrayList<>();
		List<String> metaDesc=new ArrayList<>();
		List<ProductReview> resultList = new ArrayList<>();
		InputStream in;
		File aDirectory = new File("C:\\Users\\Lbedoucha Conseil\\Dropbox\\work" +
				"\\Niching\\fiches produit\\vague finale poste correction liantsoa" +
				"\\vague 01-60 OK avec parser 2\\not bugged - done with num2");

		// get a listing of all files in the directory
		File[] filesInDir = aDirectory.listFiles();

		// sort the list of files (optional)
		// Arrays.sort(filesInDir);

		// have everything i need, just print it now
		for ( int i=0; i<filesInDir.length; i++ ) {
			System.out.println(filesInDir[i]);
			if (!filesInDir[i].getName().endsWith(".docx") | filesInDir[i].getName().startsWith("~$"))
				continue;
			try {
				String path = "D:\\freelance\\2018\\docx2html_parse\\bugged\\" + "24 fiches produit secateur electrique.docx";
				in = new FileInputStream(filesInDir[i]);//in = new FileInputStream(new File(path));
				XWPFDocument document = new XWPFDocument(in);
				// using XWPFWordExtractor Class
				// XWPFWordExtractor xdoc = new XWPFWordExtractor(document);
				XHTMLOptions options = XHTMLOptions.create().URIResolver(new FileURIResolver(new File("word/media")));

				OutputStream out = new ByteArrayOutputStream();

				XHTMLConverter.getInstance().convert(document, out, options);
				String HTMLSTring = out.toString();
				Document html = Jsoup.parse(HTMLSTring);

				Elements links = html.select("p");

				List<List<Element>> productReviews = new ArrayList<>();
				List<Element> products = null;
				for (Element link : links) {
					if (link.text().trim().startsWith("Fiches produits") || link.text().trim().startsWith("Fiche produit")
							|| link.text().trim().startsWith("Fiches produit")
							|| link.toString().contains("<span class=\"Normal Titre1\">Fiche")) {
						products = new ArrayList<>();
						productReviews.add(products);
					}
					if (!link.text().trim().isEmpty())
						products.add(link);
				}

				System.out.println(productReviews.size());
				int n = 0;
				for (List<Element> elements : productReviews) {

					n++;
					ProductReview productReview = new ProductReview();
					resultList.add(productReview);
					Iterator<Element> itr = elements.iterator();
					String key = itr.next().text();
					productReview.setASIN(AsinGetter.asinGetter(key));
					if(!AsinGetter.asinGetter(key).startsWith("B")){
						System.out.println(" ##### "+key+" "+AsinGetter.asinGetter(key));
					}
					productReview.setFirstTitle(itr.next().toString());
					String title2 = itr.next().toString();
					title2 = title2.replaceFirst("<p class=\"Normal Titre2\" style=\"text-align:justified;\">", "<h2>");
					if (title2.contains("<p class=\"Normal Titre2\">")) {
						title2 = title2.replaceFirst("<p class=\"Normal Titre2\">", "<h2>");
					}
					title2 = title2.replace("</p>", "</h2>");
					productReview.setSecondTitle(title2);
					itr.next();
					productReview.setMetaDescription(itr.next().toString());
					boolean isDone = false;
					itr.next();
					do {
						if (!isDone) {
							Element element = itr.next();
							//System.out.println(element.toString());
							// if (!element.text().trim().replaceAll("\\W",
							// "").equalsIgnoreCase("nombredevoies")) {
							if (!element.toString().startsWith("<p class=\"Normal Titre2\"") && !element.toString().startsWith("<p class=\"Normal Heading2\">")) {
								productReview.setIntro(productReview.getIntro() + element.toString());
							} else {
								productReview.setMainText(element.toString());
								isDone = true;

							}
						}
					} while (itr.hasNext() && !isDone);
					isDone = false;
					do {
						Element element = itr.next();
						if (!element.text().trim().replaceAll("\\W", "").equalsIgnoreCase("Avantages")
								&& !element.text().trim().replaceAll("\\W", "").equalsIgnoreCase("Avantage")
								&& !element.text().trim().replaceAll("\\W", "").equalsIgnoreCase("Avantagese")) {
							productReview.setMainText(productReview.getMainText() + element.toString());
						} else {
							isDone = true;

						}
					} while (itr.hasNext() && !isDone);
					isDone = false;
					productReview.setAdvantages("");
					do {
						Element element = itr.next();
						if (!element.text().trim().replaceAll("\\W", "").equalsIgnoreCase("Inconvnients")
								&& !element.text().trim().replaceAll("\\W", "").equalsIgnoreCase("Inconvnient")) {
							productReview.setAdvantages(productReview.getAdvantages() + element.toString());
						} else {
							isDone = true;

						}
					} while (itr.hasNext() && !isDone);
					isDone = false;
					productReview.setDrawbacks("");
					/*
					 * while(itr.hasNext()) { Element element = itr.next();
					 * productReview.setDrawbacks(productReview.getDrawbacks() +
					 * element.toString()); }
					 */

					do {
						if (!isDone) {
							Element element = itr.next();
							// if (!element.text().trim().replaceAll("\\W",
							// "").equalsIgnoreCase("nombredevoies")) {
							if (!element.toString().startsWith("<p class=\"Normal Titre2\"")) {
								productReview.setDrawbacks(productReview.getDrawbacks() + element.toString());
							} else {
								isDone = true;

							}
						}
					} while (itr.hasNext() && !isDone);
					if(!metaDesc.contains(Jsoup.parse(productReview.getMetaDescription()).text())) {
						DbInserter.insertInDb(productReview, conn, getCount(asins, productReview.getASIN()));
						asins.add(productReview.getASIN());
						metaDesc.add(Jsoup.parse(productReview.getMetaDescription()).text());
					}else{
						System.out.println("esquive "+productReview.getASIN());
					}
				}
				resultList.forEach(model -> {
					print(model);
				});

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private static int getCount(List<String> asins,String asin){
		int ret=0;
		for(String cour:asins){
			if(cour.contains(asin)){
				ret++;
			}
		}
		return ret;
	}
	private static void print(ProductReview model) {
		if(!model.getASIN().startsWith("B")){
			System.out.println("*** PROBLEM ASIN ****"+Jsoup.parse(model.getFirstTitle()).text());
			System.out.print("&& ASIN ==  "+model.getASIN() +"\n\n");
		}
		if(model.getIntro().length()<5){
			System.out.println("*** PROBLEM INTRO ****"+Jsoup.parse(model.getFirstTitle()).text());
		}
		if(model.getAdvantages().length()<5){
			System.out.println("*** PROBLEM ADV ****");
		}
		if(model.getDrawbacks().length()<5){
			System.out.println("*** PROBLEM DRAW ****");
		}
		if(model.getMainText().length()<5){
			System.out.println("*** PROBLEM MAIN ****");
		}
		if(model.getFirstTitle().length()<5){
			System.out.println("*** PROBLEM TITLE1 ****");
		}
		if(model.getMetaDescription().length()<5){
			System.out.println("*** PROBLEM MetaDesc ****");
		}
		if(model.getSecondTitle().length()<5){
			System.out.println("*** PROBLEM TITLE2 ****");
		}
		if(!model.getMainText().contains("avis")){
			System.out.println("*** PROBLEM notre avis ****"+Jsoup.parse(model.getFirstTitle()).text()+ " "+model.getMetaDescription());
		}
		/*
		System.out.println("getASIN: " + model.getASIN());
		System.out.println("getFirstTitle: " + model.getFirstTitle());
		System.out.println("getSecondTitle: " + model.getSecondTitle());
		System.out.println("getMetaDescription: " + model.getMetaDescription());
		System.out.println("getIntro: " + model.getIntro());
		System.out.println("getMainText: " + model.getMainText());
		System.out.println("getAdvantages: " + model.getAdvantages());
		System.out.println("getDrawbacks: " + model.getDrawbacks());
		System.out.println("------------------------------------------------------------------------------------------------------------------------------------------");
		*/

	}

}
